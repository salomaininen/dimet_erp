# -*- coding: utf-8 -*-
from openerp import models, fields, api, _, tools, exceptions
import base64
import datetime

# Logger for debug
import logging
_logger = logging.getLogger(__name__)


##################
# Parser 1c parsed_file and import to ODOO#
##################
class DMonec(models.TransientModel):
    _name = 'dm.onec'

    parsed_files = fields.Many2many("ir.attachment", string="Files to proceed")
    note = fields.Text(string="Notes", translate=False)
    log = fields.Text(string="Log", translate=False)
    update_existing = fields.Boolean(string="Update existing records. Only comments will be updated!")

    @api.multi
    def button_import_payments_1c(self):
        self.ensure_one()
        # our company's details
        our_company = self.env.user.company_id
        our_company_id = our_company.id
        our_inn = our_company.partner_id.inn_d
        log = ""

        log = u"Запуск импорта\n"
        self.note = u"Содержимое успешно проанализированных файлов:\n\n"

        for parsed_file in self.parsed_files:
            log += u"Синтаксический анализа файла " + parsed_file.name + "\n"

            # parsed_file.datas not found, example URL or empty parsed_file
            if not parsed_file.datas:
                log += u'Ошибка чтения файла, нет данных, возможно файл пуст или является url\n'
                continue

            # parsed_file contains extraneous data
            try:
                import_parsed_file_text = base64.decodestring(parsed_file.datas).decode('cp1251')
            except Exception:
                log += u'Ошибка чтения файла, файл не в кодировке cp1251\n'
                continue

            # set note source of parsed_file
            self.note += import_parsed_file_text + "\n\n"

            list_import_parsed_file = import_parsed_file_text.splitlines()

            # default values
            count_all = 0
            count_payments = 0
            count_update = 0
            payment_open = False

            for line in list_import_parsed_file:

                if line == u'СекцияДокумент=Платежное поручение' or line == u'СекцияДокумент=Банковский ордер'\
                        or line == u'СекцияДокумент=Инкассовое поручение':
                    count_all += 1
                    payment_open = True
                    vals = {'company_id': our_company_id}
                    account_number_receipt = ""
                    account_number_payment = ""
                    account_number = ""
                    inn_d = ""
                    type_payment = 0
                    payment_source_text = ""
                    duplicate = False

                if line == u'КонецДокумента':

                    payment_open = False

                    # inn not found in source parsed_file
                    if type_payment == 0:
                        log += u'    Плательщик и получатель не найдены\n' + str(vals).decode(
                            'unicode-escape') + '\n'
                        continue

                    # date not found in source parsed_file
                    if not vals.get('date', False):
                        log += u'    Поле с датой не найдено\n' + str(vals).decode('unicode-escape') + '\n'
                        continue

                    # found partner inn most be unique
                    if self.env['res.partner'].sudo().search_count(
                            [('inn_d', '=', inn_d)]) > 1:
                        log += u'    Один ИНН в нескольких партнерах\n' + str(vals).decode('unicode-escape') + '\n'
                        continue

                    # add account.journal with account_number from source parsed_file
                    account_number = account_number_payment if type_payment == 1 else account_number_receipt

                    # found journal account_number most be unique
                    journal_cnt = self.env['account.journal'].sudo().search_count(
                        [('account_number', '=', account_number)])
                    if journal_cnt > 1:
                        log += u'    Один счет в нескольких журналах\n' + str(vals).decode('unicode-escape') + '\n'
                        continue

                    # journal not found
                    elif journal_cnt < 1:
                        log += u'    Не найден журнал по номеру в платеже\n' + str(vals).decode(
                            'unicode-escape') + '\n'
                        continue

                    journal_obj = self.env['account.journal'].sudo().search(
                        [('account_number', '=', account_number)])

                    vals['type'] = 'receipt' if type_payment == 1 else 'payment'

                    if vals['type'] == 'receipt':
                        vals['account_id'] = journal_obj.default_debit_account_id.id
                    else:
                        vals['account_id'] = journal_obj.default_credit_account_id.id

                    vals['journal_id'] = journal_obj.id

                    # add partner with inn from source parsed_file
                    partner_id = self.env['res.partner'].sudo().search(
                        [('inn_d', '=', inn_d)]).id
                    vals['partner_id'] = partner_id

                    # Search for duplicates
                    domain_duplicate = []
                    if partner_id:
                        domain_duplicate = ['&', '&', '&',
                                            ('reference', '=', vals['reference']),
                                            ('amount', '=', vals['amount']),
                                            ('date', '=', vals['date']),
                                            ('partner_id', '=', partner_id)]
                    else:
                        domain_duplicate = ['&', '&',
                                            ('reference', '=', vals['reference']),
                                            ('amount', '=', vals['amount']),
                                            ('date', '=', vals['date'])]

                    duplicate = self.env['account.voucher'].sudo().search(domain_duplicate) or False
                    if duplicate:

                        # Skip if not updating
                        if not self.update_existing:
                            log += u'    Найден дубликат, не обновлен\n' + str(vals).decode(
                                'unicode-escape') + '\n'
                            continue
                        # If partner not set update partner
                        if not duplicate.partner_id:
                            if partner_id:
                                duplicate.write({'name': vals['name'],
                                                 'narration': payment_source_text,
                                                 'partner_id': partner_id})
                            else:
                                duplicate.write({'name': vals['name'],
                                                 'narration': payment_source_text})
                        else:
                            duplicate.write({'name': vals['name'],
                                             'narration': payment_source_text})

                        count_update += 1
                        log += u'    Найден дубликат, обновлены name и narration\n' + str(vals).decode(
                            'unicode-escape') + '\n'

                    else:
                        # Omit extra write
                        vals['narration'] = payment_source_text
                        _logger.info("Voucher vals = %s", vals)
                        voucher = self.env['account.voucher'].create(vals)
                        # Try to find linked order
                        ctx = self._context.copy()
                        ctx.update({'notify_so': 'auto'})
                        # ctx.update({'notify_so': False})
                        voucher.with_context(ctx).find_linked_order()

                        count_payments += 1
                        log += u"Платеж из файла " + parsed_file.name + u" импортирован\n" + str(vals).decode(
                            'unicode-escape') + "\n"

                if payment_open:
                    statement = line.split('=')
                    if statement[0] == u"Номер":
                        payment_source_text += line + "\n"
                        vals['reference'] = statement[1]

                    if statement[0] == u"ПлательщикСчет":
                        payment_source_text += line + "\n"
                        account_number_receipt = statement[1]

                    if statement[0] == u"Дата":
                        payment_source_text += line + "\n"
                        date_detected = vals['date'] = datetime.datetime.strptime(statement[1], '%d.%m.%Y').date().strftime('%Y-%m-%d')
                        vals['period_id'] = self.env['account.period'].with_context(
                            {'company_id': our_company_id}).find(date_detected).id



                    if statement[0] == u"ДатаСписано" or statement[0] == u"ДатаПоступило":
                        # format default %m/%d/%Y, but is not requered for sql operation
                        payment_source_text += line + "\n"
                        vals['date'] = datetime.datetime.strptime(statement[1], '%d.%m.%Y').date().strftime('%Y-%m-%d')

                    if statement[0] == u"ПлательщикИНН":
                        payment_source_text += line + "\n"
                        if statement[1] != our_inn:
                            inn_d = statement[1]
                        else:
                            type_payment = 2  # исх

                    if statement[0] == u"ПолучательСчет":
                        payment_source_text += line + "\n"
                        account_number_payment = statement[1]

                    if statement[0] == u"ПолучательИНН":
                        payment_source_text += line + "\n"
                        if statement[1] != our_inn:
                            inn_d = statement[1]
                        else:
                            type_payment = 1  # вхо

                    if statement[0] == u"НазначениеПлатежа":
                        payment_source_text += line + "\n"
                        vals['name'] = statement[1]

                    if statement[0] == u"Сумма":
                        payment_source_text += line + "\n"
                        vals['amount'] = statement[1]

                    if statement[0] == u"Получатель1":
                        payment_source_text += line + "\n"

                    if statement[0] == u"Плательщик1":
                        payment_source_text += line + "\n"

            log += (u"Синтаксический анализа файла " + parsed_file.name + u" завершен\n" +
                         u"найдено " + str(count_all).decode("utf-8") + u" платежа(ей)\n" +
                         u"обновлено " + str(count_update).decode("utf-8") + u" платежа(ей)\n" +
                         u"импортировано " + str(count_payments).decode("utf-8") + u" платежа(ей)" +
                         "\n\n\n")
        log += u"Импорт завершен!\n"
        self.log = log
        log = ""

    @api.multi
    def vouchers_update(self):
        """ For all voucher where partner_id == False,
            read narration and search inn_d in res.partner
        """

        voucher_list = self.env['account.voucher'].sudo().search([('partner_id', '=', False)])
        log = (u"Обновление данных из поля narration, всего ваучеров без партнеров " +
               str(len(voucher_list)).decode("utf-8") + "\n\n\n")

        v_upd_count = 0
        for voucher in voucher_list:

            string_equal = u"ПлательщикИНН" if voucher.type == 'receipt' else u"ПолучательИНН"
            for line in voucher.narration.splitlines():

                statement = line.split('=')
                if statement[0] == string_equal:
                        # TODO omit write if partner not found!
                        partner_id = self.env['res.partner'].sudo().search([('inn_d', '=', statement[1])]).id or False
                        if partner_id:
                            voucher.sudo().write({'partner_id': partner_id})
                            if not voucher.auto_validated:
                                voucher.find_linked_order()
                            voucher_name = (voucher.number + ' ' + voucher.name) if voucher.number else voucher.name
                            log += (u"Обновлено значение партнера в ваучере " + voucher_name + " \n" + voucher.type + "\n\n")
                            v_upd_count += 1
                        break

        log += (u"Всего ваучеров без партнеров: " + str(len(voucher_list)).decode("utf-8") + "\n" +
                u"Обновлено: " + str(v_upd_count).decode("utf-8") )
        self.log = log
