from openerp import models, fields, api


class DMTricks(models.Model):
    _name = 'odoo.tricks'

# -- STARTUP FUNCTION ---
# -- Make countries updatable
    @api.model
    def _make_countries_updatable(self):
        records = self.env['ir.model.data'].search([('model', '=', 'res.country')])
        records.write({'noupdate':False})
        return

# -- Make countries readonly
    @api.model
    def _make_countries_readonly(self):
        records = self.env['ir.model.data'].search([('model', '=', 'res.country')])
        records.write({'noupdate':True})
        return
