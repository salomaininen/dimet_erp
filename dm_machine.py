from openerp import models, fields, api, _
# import logging
# Logger for debug
# _logger = logging.getLogger(__name__)


#################
# Machine brand #
#################
class DMMachineBrand(models.Model):
    _name = 'dm.machine.brand'

    # Fields
    name = fields.Char(string="Name", required=True)
    material_handlers = fields.One2many(string="Material handlers", comodel_name='dm.material.handler',
                                        inverse_name='brand', auto_join=True)
    material_handler_count = fields.Integer(string="Material handlers", compute='_count_material_handlers')
    attachments = fields.One2many(string="Attachments", comodel_name='dm.machine.attachment',
                                  inverse_name='brand', auto_join=True)
    attachment_count = fields.Integer(string="Attachments", compute='_count_attachments')

    # Order
    _order = 'name'

    # SQL constraints
    _sql_constraints = [('machine_brand_unique',
                         'UNIQUE (name)',
                         _('This brand already exists!'))]

# - Open material handlers
    @api.multi
    def act_material_handlers(self):
        self.ensure_one()
        brand = self.id
        ctx = {
            'default_brand': brand
        }

        return {
            'name': _("Material handlers"),
            'view_mode': 'tree,form',
            'res_model': 'dm.material.handler',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'context': ctx,
            'domain': [('brand', '=', brand)]
        }

# -- Count material handlers -- #
    @api.onchange('material_handlers')
    @api.multi
    def _count_material_handlers(self):
        for rec in self:
            rec.material_handler_count = self.env['dm.material.handler'].search_count([('brand','=',rec.id)])


# - Open attachments
    @api.multi
    def act_attachments(self):
        self.ensure_one()
        brand = self.id
        ctx = {
            'default_brand': brand
        }

        return {
            'name': _("Attachments"),
            'view_mode': 'tree,form',
            'res_model': 'dm.machine.attachment',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'context': ctx,
            'domain': [('brand', '=', brand)]
        }

# -- Count attachments -- #
    @api.onchange('attachments')
    @api.multi
    def _count_attachments(self):
        for rec in self:
            rec.attachment_count = self.env['dm.machine.attachment'].search_count([('brand','=',rec.id)])


####################
# Material handler #
####################
class DMMaterialHandler(models.Model):
    _name = 'dm.material.handler'

    # Fields
    name = fields.Char(string="Name", required=True)
    brand = fields.Many2one(string="Brand", comodel_name='dm.machine.brand')
    brand_name = fields.Char(string="Brand", related='brand.name')
    variants = fields.One2many(string="Variants", comodel_name='dm.material.handler.variant',
                               inverse_name='material_handler', auto_join=True)
    variant_count = fields.Integer(string="Variants", compute='_count_variants')

    # Order
    _order = 'brand, name'

    # SQL constraints
    _sql_constraints = [('material_handler_name_brand_unique',
                         'UNIQUE (name, brand)',
                         _('Such model for this brand already exists!'))]


# - Open variants
    @api.multi
    def act_variants(self):
        self.ensure_one()
        material_handler = self.id
        ctx = {
            'default_material_handler': material_handler
        }

        return {
            'name': _("Variants"),
            'view_mode': 'tree,form',
            'res_model': 'dm.material.handler.variant',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'context': ctx,
            'domain': [('material_handler', '=', material_handler)]
        }

# -- Count variants -- #
    @api.onchange('variants')
    @api.multi
    def _count_variants(self):
        for rec in self:
            rec.variant_count = self.env['dm.material.handler.variant'].search_count([('material_handler','=',rec.id)])


############################
# Material handler variant #
############################
class DMMaterialHandlerVariant(models.Model):
    _name = 'dm.material.handler.variant'
    _rec_name = 'complete_name'

    # Fields
    material_handler = fields.Many2one(string="Material handler", comodel_name='dm.material.handler', required=True)
    name = fields.Char(string="Material handler", related='material_handler.name')
    complete_name = fields.Char(string="Model", compute='_get_complete_name')
    brand_name = fields.Char(string="Brand", related='material_handler.brand_name')
    variant_name = fields.Char(string="Variant code", translate=True)
    boom_length = fields.Float(string="Boom length(m)", digits=(5,2), required=True)
    stick_length = fields.Float(string="Stick length(m)", digits=(5,2), required=True)
    capacity_min = fields.Integer(string="Lifting capacity min (kg)", required=True)
    attachments = fields.Many2many(comodel_name='dm.machine.attachment',
                                   relation='mhv_attachment',
                                   column2='att_id',
                                   column1='mhv_id',
                                   string="Compatible attachments")

    # Order
    _order = 'boom_length, stick_length'

    # SQL constraints
    _sql_constraints = [('material_handler_variant_unique',
                         'UNIQUE (material_handler, boom_length, stick_length, variant_name)',
                         _('Such variant for this model already exists!'))]

# -- Get complete name -- #
    @api.multi
    def _get_complete_name(self):
        for rec in self:
            variant_name = (' ' + rec.variant_name + ' ') if rec.variant_name else ''
            rec.complete_name = rec.brand_name + ' ' + rec.name + variant_name + _(' boom ') + str(rec.boom_length) + _(' stick ') + str(rec.stick_length)


###########################
# Machine attachment type #
###########################
class DMMachineAttachmentType(models.Model):
    _name = 'dm.machine.attachment.type'

    # Fields
    name = fields.Char(string="Name", required=True, translate=True)
    attachments = fields.One2many(string="Attachments", comodel_name='dm.machine.attachment',inverse_name='type', auto_join=True)
    magnet_adapters = fields.One2many(string="Magnet adapters", comodel_name='dm.magnet.adapter',
                                      inverse_name='attachment_type', auto_join=True)


#######################
# Machine attachments #
#######################
class DMMachineAttachment(models.Model):
    _name = 'dm.machine.attachment'
    _rec_name = 'complete_name'

    # Fields
    name = fields.Char(string="Name", required=True, translate=True)
    type = fields.Many2one(string="Type", required=True, comodel_name='dm.machine.attachment.type')
    type_name = fields.Char(string="Type", related='type.name')
    complete_name = fields.Char(string="Model", compute='_get_complete_name')
    brand = fields.Many2one(string="Brand", comodel_name='dm.machine.brand')
    brand_name = fields.Char(string="Brand", related='brand.name')
    weight = fields.Integer(string="Weight (kg)", required=True)
    power_required = fields.Float(string="Power ", digits=(5,2), required=False)
    mh_variants = fields.Many2many(comodel_name='dm.material.handler.variant',
                                   relation='mhv_attachment',
                                   column1='att_id',
                                   column2='mhv_id',
                                   string="Compatible material handlers")

    # Order
    _order = 'brand, name'

    # SQL constraints
    _sql_constraints = [('machine_attachment_unique',
                         'UNIQUE (name, brand, weight)',
                         _('Such attachment already exists!'))]

# -- Get complete name -- #
    @api.multi
    def _get_complete_name(self):
        for rec in self:
            brand_name = rec.brand_name
            if brand_name:
                rec.complete_name = brand_name + ' ' + rec.name + _(' weight ') + str(rec.weight) + 'kg'
            else:
                rec.complete_name = rec.name + _(' weight ') + str(rec.weight) + 'kg'

