openerp.dimet_erp = function(instance) {
    instance.web.WebClient.include({
        init: function(parent, client_options) {
            this._super(parent, client_options);
            this.set('title_part', {"zopenerp": "Dimet"});
        },
        set_title: function(title) {
          title = _.str.clean(title);
          var sep = _.isEmpty(title) ? '' : ' - ';
          document.title = title + sep + 'Dimet';
        },
    });
};
