from openerp import models, fields, api, _


#####################
# Material standard #
#####################
class DMMaterialStandard(models.Model):
    _name = 'dm.materialstandard'
    _description = 'Material standard'

    # Fields
    name = fields.Char(string="Name", required=True, translate=True)

    # SQL constraints
    _sql_constraints = [('material_standard_unique',
                         'UNIQUE (name)',
                         _('This material standard already exists!'))]

#################
# Material type #
#################
class DMMaterialType(models.Model):
    _name = 'dm.materialtype'
    _description = 'Material type'

    # Fields
    name = fields.Char(string="Name", required=True, translate=True)
    standard_id = fields.Many2one(string="Standard", comodel_name='dm.materialstandard')

    # SQL constraints
    _sql_constraints = [('material_type_unique',
                         'UNIQUE (name)',
                         _('This material type already exists!'))]


############
# Material #
############
class DMMaterial(models.Model):
    _name = 'dm.material'
    _description = 'Material base class'

    # Fields
    name = fields.Char(string="Name")
    material_type_id = fields.Many2one(string="Material type", comodel_name='dm.materialtype', required=True)
    standard_id = fields.Many2one(string="Standard", comodel_name='dm.materialstandard')
    product_id = fields.Many2one(string="Related product")

    # SQL constraints
    _sql_constraints = [('material_unique',
                         'UNIQUE (product_id)',
                         _('Material can be used only for with a single product!'))]


#########
# Sheet #
#########
class DMMaterialSheet(models.Model):
    _name = 'dm.material.sheet'
    _inherit = 'dm.material'
    _description = _('Sheet')

    # Fields
    length = fields.Integer(string="Length(mm)", required=True)
    width = fields.Integer(string="Width(mm)", required=True)
    thickness = fields.Integer(string="Thickness(mm)", required=True)

    # SQL constraints
    _sql_constraints = [('material_sheet_unique',
                         'UNIQUE (standard_id,material_type_id,length,width,thickness)',
                         _('Such material already exists!'))]

# -- Create
    @api.model
    def create(self, vals):

        type_name = False
        standard_name = False

        # Get material type name and add it to name
        if 'material_type_id' in vals:
            if vals['material_type_id']:
                type_name = self.env['dm.materialtype'].search([("id", '=', vals['material_type_id'])]).name
                name = self._description + ' ' + type_name
        else:
            name = self._description

        # Get material standard name
        if 'standard_id' in vals:
            if vals['material_type_id']:
                standard_name = self.env['dm.materialstandard'].search([("id", '=', vals['standard_id'])]).name

        # Compose name
        name += ' ' + str(vals['length']) + '*' + str(vals['width']) + '*' + str(vals['thickness'])
        if standard_name:
            name += ' ' + standard_name

        vals['name'] = name

        # Compose record
        return super(DMMaterialSheet, self).create(vals)
