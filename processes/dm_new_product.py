from openerp import models, fields, api, _, tools, SUPERUSER_ID
from datetime import datetime

# import logging
# Logger for debug
# _logger = logging.getLogger(__name__)


######################
# New product design #
######################
class DMNewProduct(models.Model):
    _name = 'dm.bp.np'
    _description = "Product design request"
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    # Fields
    state = fields.Selection([
        ('0', 'Draft'),
        ('1', 'Confirmed'),
        ('a', 'Accepted'),
        ('o', 'To redo'),
        ('p', 'In progress'),
        ('s', 'Suspended'),
        ('e', 'Complete'),
        ('r', 'Price confirmed'),
        ('d', 'Done'),
        ('x', 'Declined'),
        ('c', 'Cancelled'),
    ], string="State", default='0', required=True, track_visibility='onchange')

    # Dates and priority
    date_deadline = fields.Date(string="Deadline", required=True, track_visibility='onchange')
    dt_confirmed = fields.Datetime(string="Confirmed", readonly=1)
    dt_accepted = fields.Datetime(string="Accepted", readonly=1)
    dt_work_started = fields.Datetime(string="Work started", readonly=1)
    dt_work_complete = fields.Datetime(string="Work complete", readonly=1)
    dt_price_confirmed = fields.Datetime(string="Price confirmed", readonly=1)
    dt_done = fields.Datetime(string="All done", readonly=1)
    dt_cancelled = fields.Datetime(string="Cancelled", readonly=1)
    dt_declined = fields.Datetime(string="Declined", readonly=1)
    days_to_deadline = fields.Char(string="Days to deadline", compute='days_left')
    warn_level = fields.Integer(string="Warning level", compute="_dummy")

    # Gantt view
    gantt_start = fields.Date(string="Gantt start", compute="_get_gantt_start")
    gantt_end = fields.Date(string="Gantt end", compute="_get_gantt_end")
    gantt_progress = fields.Integer(string="Gantt progress", compute="_get_gantt_progress")

    priority = fields.Selection([
        ('0', 'Low'),
        ('1', 'Regular'),
        ('2', 'High'),
    ], string="Priority", default='1', required=True, track_visibility='onchange')

    # General data
    name = fields.Char(string="Brief description", required=True, translate=True)
    opportunity_id = fields.Many2one(string="Opportunity", comodel_name='crm.lead', required=True,
                                     groups='base.group_sale_salesman')
    partner_id = fields.Many2one(string="Partner", comodel_name='res.partner', required=True)
    company_id = fields.Many2one(string="Company", comodel_name='res.company', required=True,
                                 default=lambda self: self.env['res.users'].browse([self._uid]).company_id)
    products_requested = fields.Many2many(string="Requested products", comodel_name='dm.product',
                                          relation='dm_product_np_rel',
                                          column1='npd',
                                          column2='product')
    # In
    note_in = fields.Text(string="Description, inbound", translate=True)
    documents_in = fields.Many2many(string="Inbound documents", comodel_name='ir.attachment',
                                    relation='dm_bp_np_ir_attachment_in_rel',
                                    column1='npd',
                                    column2='attachment')
    suggested_products = fields.One2many(string="Suggested products", comodel_name='dm.bp.np.product.suggested',
                                         inverse_name='np_id')
    # Out
    note_out = fields.Text(string="Description, outbound", translate=True)
    documents_out = fields.Many2many(string="Outbound documents", comodel_name='ir.attachment',
                                     relation='dm_bp_np_ir_attachment_out_rel',
                                     column1='npd',
                                     column2='attachment')
    related_products = fields.One2many(string="Output products", comodel_name='dm.bp.np.line',
                                       inverse_name='np_id')
    # Delegation
    assigned_id = fields.Many2one(string="Assigned to", comodel_name='res.users', track_visibility='onchange')
    # Messages
    message_notify = fields.Char(string="Message notification", compute='_message_notify')

    # Order
    _order = 'priority desc,id desc'

    # SQL constraints
    _sql_constraints = [('dm_np_unique',
                         'UNIQUE (name)',
                         _('Such task already exists!'))]

    _track = {
        'state': {'dimet_erp.dm_np_state_changed': lambda self, cr, uid, obj, ctx=None: obj.state},
        'priority': {'dimet_erp.dm_np_priority_changed': lambda self, cr, uid, obj, ctx=None: obj.priority},
        'date_deadline': {
            'dimet_erp.dm_np_date_deadline_changed': lambda self, cr, uid, obj, ctx=None: obj.date_deadline},
        'assigned_id': {'dimet_erp.dm_np_assigned': lambda self, cr, uid, obj, ctx=None: obj.assigned_id}

    }

    # -- Opportunity change
    @api.multi
    @api.onchange('opportunity_id')
    def opportunity_change(self):
        self.ensure_one()
        partner_ids = []
        opportunity_partner_id = self.opportunity_id.partner_id
        current_id = opportunity_partner_id.id
        self.partner_id = current_id
        # Add partner id first
        partner_ids.append(current_id)
        # Check if has parent
        if opportunity_partner_id.parent_id:
            partner_ids.append(opportunity_partner_id.parent_id.id)
        # Get children
        children_ids = self.env['res.partner'].search([('parent_id', '=', current_id)]).ids
        if children_ids:
            partner_ids += children_ids
        #        _logger.info("Partner ids = %s", partner_ids)
        return {
            'domain': {'partner_id': [('id', 'in', partner_ids)]}
        }

    # -- Get gantt start
    @api.multi
    def _get_gantt_start(self):
        for rec in self:
            if rec.dt_confirmed:
                rec.gantt_start = rec.dt_confirmed
        return

    # -- Get gantt end
    @api.multi
    def _get_gantt_end(self):
        date_now = fields.Date.today()

        for rec in self:
            if rec.state == 'd':
                rec.gantt_end = rec.dt_done
            elif rec.state == 'x':
                rec.gantt_end = rec.dt_declined
            elif rec.state == 'c':
                rec.gantt_end = rec.dt_cancelled

            else:
                deadline = rec.date_deadline
                if date_now > deadline:
                    rec.gantt_end = date_now
                else:
                    rec.gantt_end = deadline

                # -- Get gantt progress

    @api.multi
    def _get_gantt_progress(self):
        for rec in self:
            if rec.state == 'a':
                rec.gantt_progress = 10
            elif rec.state == 'p':
                rec.gantt_progress = 55
            elif rec.state == 'e':
                rec.gantt_progress = 75
            elif rec.state == 'r':
                rec.gantt_progress = 90
            elif rec.state in ['d', 'c', 'x']:
                rec.gantt_progress = 100
            else:
                rec.gantt_progress = 0


# -- Show message notification

    def _message_notify(self):
        for rec in self:
            rec.message_notify = "New!" if rec.message_unread else False

        # -- Dummy function for computed fields

    @api.multi
    def _dummy(self):
        return

    # -- Get days left until deadline and date miss
    @api.multi
    def days_left(self):
        date_format = tools.DEFAULT_SERVER_DATE_FORMAT
        date_now = datetime.today()
        for rec in self:
            days_left = (datetime.strptime(rec.date_deadline, date_format) - date_now).days
            days_to_deadline = str(days_left) if rec.state in ['a', 'p', 's', 'o'] else False
            rec.days_to_deadline = days_to_deadline
            if days_to_deadline:
                if days_left < 2:
                    rec.warn_level = 0
                elif days_left < 4:
                    rec.warn_level = 1


                # -- Override message post TODO fixed with access rights now but maybe come back later

    @api.v7
    def temp_message_post(self, cr, uid, thread_id, body='', subject=None, type='notification',
                          subtype=None, parent_id=False, attachments=None, context=None,
                          content_subtype='html', **kwargs):
        kwargs['author_id'] = self.pool.get('res.users').browse(cr, SUPERUSER_ID, uid, context=None).partner_id.id
        res = super(DMNewProduct, self).message_post(cr, SUPERUSER_ID, thread_id, body, subject, type,
                                                     subtype, parent_id, attachments, context,
                                                     content_subtype, **kwargs)
        return res

    # -- Create
    @api.model
    def create(self, vals):

        # _logger.info("Create vals: %s", vals)
        # Check if attachment scan is needed
        att_in = True if 'documents_in' in vals else False
        att_out = True if 'documents_out' in vals else False

        # Create!
        res = super(DMNewProduct, self).create(vals)

        # Manage attachments
        new_id = res.id
        if att_in:
            for attachment in res.documents_in:
                if not attachment.res_id:
                    attachment.write({'res_model': 'dm.bp.np', 'res_id': new_id})

        if att_out:
            for attachment in res.documents_out:
                if not attachment.res_id:
                    attachment.write({'res_model': 'dm.bp.np', 'res_id': new_id})
                    # Add attachment to crm.lead from related opportunity_id field
                    # attachment.write({'res_model': 'crm.lead', 'res_id': res.opportunity_id})

        return res

    # -- Write
    @api.multi
    def write(self, vals):
        # Log

        # Check if attachment scan is needed
        att_in = True if 'documents_in' in vals else False
        att_out = True if 'documents_out' in vals else False

        # Subscribe person task assigned to if not already assigned
        if 'assigned_id' in vals and vals['assigned_id']:
            assigned_partner = self.env['res.users'].sudo().browse([vals['assigned_id']]).partner_id
            if assigned_partner:
                assigned_partner_id = assigned_partner.id
                for rec in self:
                    if assigned_partner_id not in rec.message_follower_ids.ids:
                        rec.message_subscribe(partner_ids=[assigned_partner_id])
        # Write!
        res = super(DMNewProduct, self).write(vals)

        # Manage attachments
        if att_in:
            for rec in self:
                for attachment in rec.documents_in:
                    if not attachment.res_id:
                        attachment.write({'res_model': 'dm.bp.np', 'res_id': rec.id})

        if att_out:
            for rec in self:
                for attachment in rec.documents_out:
                    if not attachment.res_id:
                        attachment.write({'res_model': 'dm.bp.np', 'res_id': rec.id})
                        # Add attachment to crm.lead from related opportunity_id field
                        # attachment.write({'res_model': 'crm.lead', 'res_id': rec.opportunity_id})

        return res

    # - Confirm -
    @api.multi
    def button_confirm(self):
        return {
            'type': 'ir.actions.act_window.message',
            'title': _('Confirm your action'),
            'message': _('By pressing "YES" you will confirm this request. \n'
                         'Initial data cannot be modified beyond this point!'
                         '\n Use Messages and Internal Notes to add information and attachments'),
            'close_button_title': _("No"),
            'buttons': [{
                'type': 'method',
                'name': _('Yes'),
                'model': self._name,
                'method': 'act_confirm',
                'args': [self.ids]
            }]
        }

    @api.multi
    def act_confirm(self):
        self.ensure_one()
        self.write({'state': '1', 'dt_confirmed': fields.Datetime.now()})

    # - Accept -
    @api.multi
    def button_accept(self):
        return {
            'type': 'ir.actions.act_window.message',
            'title': _('Confirm your action'),
            'message': _('By pressing "YES" you will accept this request'),
            'close_button_title': _("No"),
            'buttons': [{
                'type': 'method',
                'name': _('Yes'),
                'model': self._name,
                'method': 'act_accept',
                'args': [self.ids]
            }]
        }

    @api.multi
    def act_accept(self):
        self.ensure_one()
        self.write({'state': 'a', 'dt_accepted': fields.Datetime.now()})

    # - Start works (progress) -
    @api.multi
    def button_progress(self):
        return {
            'type': 'ir.actions.act_window.message',
            'title': _('Confirm your action'),
            'message': _('By pressing "YES" you will start work on this request'),
            'close_button_title': _("No"),
            'buttons': [{
                'type': 'method',
                'name': _('Yes'),
                'model': self._name,
                'method': 'act_progress',
                'args': [self.ids]
            }]
        }

    @api.multi
    def act_progress(self):
        self.ensure_one()
        self.write({'state': 'p', 'dt_work_started': fields.Datetime.now()})

    # - Suspend works -
    @api.multi
    def button_suspend(self):
        return {
            'type': 'ir.actions.act_window.message',
            'title': _('Confirm your action'),
            'message': _('By pressing "YES" you will suspend work on this request'),
            'close_button_title': _("No"),
            'buttons': [{
                'type': 'method',
                'name': _('Yes'),
                'model': self._name,
                'method': 'act_suspend',
                'args': [self.ids]
            }]
        }

    @api.multi
    def act_suspend(self):
        self.ensure_one()
        self.write({'state': 's'})

    # - Finish works (complete) -
    @api.multi
    def button_complete(self):
        return {
            'type': 'ir.actions.act_window.message',
            'title': _('Confirm your action'),
            'message': _('By pressing "YES" you will finish work on this request'),
            'close_button_title': _("No"),
            'buttons': [{
                'type': 'method',
                'name': _('Yes'),
                'model': self._name,
                'method': 'act_complete',
                'args': [self.ids]
            }]
        }

    @api.multi
    def act_complete(self):
        self.ensure_one()
        self.write({'state': 'e', 'dt_work_complete': fields.Datetime.now()})

    # - Confirm prices  -
    @api.multi
    def button_price_confirm(self):

        return {
            'type': 'ir.actions.act_window.message',
            'title': _('Confirm your action'),
            'message': _('By pressing "YES" you confirm prices for this order'),
            'close_button_title': _("No"),
            'buttons': [{
                'type': 'method',
                'name': _('Yes'),
                'model': self._name,
                'method': 'act_price_confirm',
                'args': [self.ids]
            }]
        }

    @api.multi
    def act_price_confirm(self):
        self.ensure_one()
        self.write({'state': 'r', 'dt_price_confirmed': fields.Datetime.now()})

    # - All done (done) -
    @api.multi
    def button_done(self):
        return {
            'type': 'ir.actions.act_window.message',
            'title': _('Confirm your action'),
            'message': _('By pressing "YES" you confirm thar all work on this request is done'),
            'close_button_title': _("No"),
            'buttons': [{
                'type': 'method',
                'name': _('Yes'),
                'model': self._name,
                'method': 'act_done',
                'args': [self.ids]
            }]
        }

    @api.multi
    def act_done(self):
        self.ensure_one()
        self.write({'state': 'd', 'dt_done': fields.Datetime.now()})

    # - Cancel request (cancel) -
    @api.multi
    def button_cancel(self):
        return self.reason_wizard('c', _("Cancel request"))

    # - Decline request -
    @api.multi
    def button_decline(self):
        return self.reason_wizard('x', _("Decline request"))

    # -- To Redo
    @api.multi
    def button_to_redo(self):
        return self.reason_wizard('o', _("To redo"))

    # -- Open reason wizard
    @api.multi
    def reason_wizard(self, action_id, reason=None):
        self.ensure_one()
        context = {
            'default_action_id': action_id,
            'model_name': 'dm.bp.np'
        }

        return {
            'name': reason,
            "views": [[False, "form"]],
            'res_model': 'dm.bp.sc.wiz',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': context
        }

    # -- Add new product
    @api.multi
    def button_np_add(self):
        self.ensure_one()

        return {
            'name': _("New product"),
            "views": [[False, "form"]],
            'res_model': 'dm.bp.np.wiz',
            'type': 'ir.actions.act_window',
            'target': 'new'
        }


###################################
# New product design product line #
###################################
class DMNewProductLine(models.Model):
    _name = 'dm.bp.np.line'
    _description = "Product line for new product design request"

    @api.model
    def _reference_models(self):
        dm_products = self.env['dm.product'].search([('id', '!=', False)])
        models = [dm_product.model_id for dm_product in dm_products]
        return [(model.model, model.name)
                for model in models]

    # Fields
    np_id = fields.Many2one(string="Product design request id", comodel_name='dm.bp.np')
    dm_product_ref = fields.Reference(string="Product", selection="_reference_models")
    qty = fields.Integer(string="Quantity", default=1)

    # Button click
    # Open related record
    @api.multi
    def button_open(self):
        self.ensure_one()

        # Exit if ref is empty
        if not self.dm_product_ref:
            return

        # Open related product
        return {
            'name': _("Designed ") + self.dm_product_ref.name,
            "views": [[False, "form"]],
            'res_model': self.dm_product_ref._name,
            'type': 'ir.actions.act_window',
            'target': 'current',
            'res_id': self.dm_product_ref.id
        }


#############################################
# New product design suggested product line #
#############################################
class DMNewProductSuggestedLine(models.Model):
    _name = 'dm.bp.np.product.suggested'
    _description = "Suggested products for new product design request"

    @api.model
    def _reference_models(self):
        dm_products = self.env['dm.product'].search([('id', '!=', False)])
        models = [dm_product.model_id for dm_product in dm_products]
        return [(model.model, model.name)
                for model in models]

    # Fields
    np_id = fields.Many2one(string="Product design request id", comodel_name='dm.bp.np')
    dm_product_ref = fields.Reference(string="Product", selection="_reference_models")
    note = fields.Text(string="Note", required=True)

    # Button click
    # Open related record
    @api.multi
    def button_open(self):
        self.ensure_one()

        # Exit if ref is empty
        if not self.dm_product_ref:
            return

        # Open related product
        return {
            'name': _("Suggested ") + self.dm_product_ref.name,
            "views": [[False, "form"]],
            'res_model': self.dm_product_ref._name,
            'type': 'ir.actions.act_window',
            'target': 'current',
            'res_id': self.dm_product_ref.id
        }


##########################
# Add new product wizard #
##########################
class DMBpNpWizard(models.TransientModel):
    _name = 'dm.bp.np.wiz'

    product = fields.Many2one(string="Product", comodel_name='dm.product', required=True)

    # - Create product -
    @api.multi
    def button_continue(self):
        self.ensure_one()
        return {
            'name': _("New product"),
            "views": [[False, "form"]],
            'res_model': self.product.model_id.model,
            'type': 'ir.actions.act_window',
            'target': 'current',
            'context': {'default_np_id': self._context.get('active_id')}
        }


###################################
# State change description wizard #
###################################
class DMBpSCWizard(models.TransientModel):
    _name = 'dm.bp.sc.wiz'

    action_id = fields.Char(string="Action id")
    reason = fields.Text(string="Reason", required=True)

    @api.multi
    def button_continue(self):
        self.ensure_one()
        bp_id = self._context.get('active_id')
        model_name = self._context.get('model_name')
        if not bp_id:
            return

        bp = self.env[model_name].browse([bp_id])
        if self.action_id == 'o':
            bp.write({'state': 'o'})
        elif self.action_id == 'x':
            bp.write({'state': 'x', 'dt_declined': fields.Datetime.now()})
        elif self.action_id == 'c':
            bp.write({'state': 'c', 'dt_cancelled': fields.Datetime.now()})

        # Write the reason
        bp.message_post(body=self.reason)
