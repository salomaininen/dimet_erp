from openerp import models, fields, api, _, tools, exceptions
import logging

_logger = logging.getLogger(__name__)

####################
#  Transport Order #
####################
VALUES = [('i', "purchase.order"), ('o', "sale.order.line"), ]
VALUES_ORDER = ["purchase.order", "sale.order"]


class DMTransportOrder(models.Model):
    """
    Transport
    """
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    # -- override name_get
    # -- TODO: Get name from Sales Order or Purchase Order

    _name = "dm.transport.order"

# -- Fields
    """""
    TODO as an option
    1. Add transport cost, transport cost taxes and transport cost with taxes fields. Visible to Purchase manager only
    2. All related Purchase Order field.
    3. Generate a new Purchase Order when transport mode is selected.
    4. Get supplier and transport cost values from related Purchase Order.
    5. When related Purchase Order value changes, change Transport Order cost. 
    """""
    name = fields.Char(string="Name")
    user_id = fields.Many2one(string="Responsible", comodel_name='res.users')
    order_id = fields.Integer(string="Source order")
    line_order_id = fields.Integer(string="Source order line")
    # relate to Lead manager from model sale.order inherit
    source_ref = fields.Reference(string='Order Source', selection='get_order')
    transport_mode = fields.Many2one(string="Transport mode", comodel_name='dm.transport.mode',
                                     track_visibility='onchange')
    product_lines = fields.One2many(comodel_name="dm.transport.order.line", inverse_name='transport_order_id',
                                    string="Transport order product line", compute="_compute_one2m_pl",
                                    ondelete="cascade")
    # Related purchase order
    po_id = fields.Many2one(string="Purchase Order for transport", comodel_name='purchase.order')
    carrier_id = fields.Many2one(string="Carrier", comodel_name='res.partner', domain=[('supplier', '=', True)],
                                 related='po_id.partner_id', readonly=True,
                                 track_visibility='onchange')
    state = fields.Selection([
        ('0', "Draft"),
        ('1', "Calculated"),
        ('2', "Confirmed"),
        ('3', "Ready"),
        ('4', "In transit"),
        ('5', "Done"),
        ('6', "Cancelled"),
    ], string="State", required=True, track_visibility='onchange')
    transport_type = fields.Selection([
        ('i', "Incoming"),
        ('o', "Outgoing"),
        ('t', "Transit"),
    ], string="Order type")

    # Partner's address
    partner_shipping_id = fields.Many2one(string='Partner', comodel_name='res.partner', readonly=True,
                                          required=True, auto_join=True)

    address_id = fields.Char(string="Address", related='partner_shipping_id.contact_address')
    street = fields.Char(string='Street', related='partner_shipping_id.street', readonly=True)
    street2 = fields.Char(string='Street2', related='partner_shipping_id.street2', readonly=True)
    zip = fields.Char(string='Zip', size=24, change_default=True, related="partner_shipping_id.zip", readonly=True)
    city = fields.Char(string='City', related='partner_shipping_id.city', readonly=True)
    state_id = fields.Many2one(string='State', ondelete='restrict', related='partner_shipping_id.state_id',
                               readonly=True)
    country_id = fields.Many2one(string='Country', ondelete='restrict', related='partner_shipping_id.country_id',
                                 readonly=True)
    location_id = fields.Many2one(string="Pick-up Location", comodel_name='stock.location', select=True,
                                  ondelete='cascade')

    # Price
    price = fields.Float(string="Price, without tax", track_visibility='onchange')
    tax_ids = fields.Many2many('account.tax', string="Taxes")
    price_tax = fields.Float(string="Price, including taxes", compute='_compute_price_tax')

    # Timeline
    dod_est = fields.Date(string="Date of departure, planned", required=False, track_visibility='onchange')
    dod_real = fields.Date(string="Date of departure, factual")
    doa_est = fields.Date(string="Date of arrival, planed", track_visibility='onchange')
    doa_real = fields.Date(string="Date of arrival, factual")
    duration_est = fields.Integer(string="Duration, planned (days)", compute='_dummy')
    duration_real = fields.Integer(string="Duration, factual (days)", compute='_dummy')

    # Weight and volume
    weight = fields.Integer(string="Weight, kg")
    volume = fields.Float(string="Volume, m3")

    # Note
    note = fields.Text(string="Notes", translate=True)

    _track = {
        'state': {'dimet_erp.dm_to_state_changed': lambda self, cr, uid, obj, ctx=None: obj.state},
        'price': {'dimet_erp.dm_to_price_changed': lambda self, cr, uid, obj, ctx=None: obj.price},
        'dod_est': {'dimet_erp.dm_to_dod_est_changed': lambda self, cr, uid, obj, ctx=None: obj.dod_est},
        'doa_est': {'dimet_erp.dm_to_doa_est_changed': lambda self, cr, uid, obj, ctx=None: obj.doa_est}
    }

# -- Change state TODO Do we really need that for related field?
    @api.multi
    def onchange_state(self, state_id):
        if state_id:
            state = self.env['res.country.state'].browse(state_id)
            return {'value': {'country_id': state.country_id.id}}
        return {}

# -- Compute price with taxes added
    @api.depends('tax_ids')
    @api.multi
    def _compute_price_tax(self):
        for rec in self:
            res = 0
            if rec.tax_ids:
                for tax in rec.tax_ids:
                    res = res + (rec.price * tax.amount)
            rec.price_tax = rec.price + res

# -- Dummy
    @api.multi
    def _dummy(self):
        """
        Dummy function
        """
        return

    @api.model
    def get_order(self):
        source_models = self.env['ir.model'].search([('model', 'in', VALUES_ORDER)])
        return [(model.model, model.name)
                for model in source_models]

# -- Create
    @api.model
    def create(self, vals):
        """
        Compose name
        If any other transport order exist for same source then name = <name> + <total count -1> else <name>
         
        """
        if 'transport_type' in vals:
            transport_type = vals['transport_type']
        else:
            return super(DMTransportOrder, self).create(vals)
        if 'order_id' in vals:
            order_id = vals['order_id']
        else:
            return super(DMTransportOrder, self).create(vals)
        name_original = vals['name'] if 'name' in vals else ''

        cnt = self.env["dm.transport.order"].sudo().search_count([('order_id', '=', order_id),
                                                                  ('transport_type', '=', transport_type)])
        if cnt > 0:
            vals['name'] = name_original + '-' + str(cnt)

        return super(DMTransportOrder, self).create(vals)


# -- Compute transport order strings
    @api.multi
    def _compute_one2m_pl(self):
        """
        Refresh dm.transport.order.line
        Delete all records related from this order to dm.transport.order.line
        Create new records in dm.transport.order.line
        """
        for obj in self:
            search_model = dict(VALUES).get(obj.transport_type, False)

            if search_model:
                order_line_ids = self.env[search_model].sudo().search([('order_id', '=', obj.order_id)])
                tl_to_del_ids = self.env["dm.transport.order.line"].sudo().search(
                    [('transport_order_id', '=', self.id)]).unlink()

                for order_line in order_line_ids:
                    if order_line.product_id.type != 'service':
                        tol_id = self.env['dm.transport.order.line'].create({
                            'transport_order_id': obj.id,
                            'product_id': order_line.product_id.id,
                            'qty': order_line.product_uom_qty,
                        })

                obj.product_lines = self.env['dm.transport.order.line'].sudo().search(
                    [('transport_order_id', '=', obj.id)])

# -- Write
    @api.multi
    def write(self, vals):
        """
        extend write function
        ADD/REWRITE to sale.order -> sle.order.line 
        record  = dm.transport.order -> transport_mode
        """
        res = super(DMTransportOrder, self).write(vals)
        # state == calculated, Confirmed, ready, in transit, done
        if vals.get('state') in ['1', '2', '3', '4', '5'] or self.state in ['1', '2', '3', '4', '5']:
            # If already exist do write else create
            # FIX sale.order.line to VALUES!!!
            if not self.transport_mode:
                raise exceptions.ValidationError("Warning! transport mode doesnt exist")
            if self.line_order_id:
                self.env['sale.order.line'].browse(self.line_order_id).write({
                    'product_id': self.transport_mode.product_id.id,
                    'name': self.transport_mode.name,
                    'order_id': self.order_id,
                    'tax_id': [(6, 0, [i.id for i in self.tax_ids])],

                    'date_planned': self.doa_est,
                    'price_unit': self.price,
                    'product_uom': self.env.ref('product.product_uom_unit').id,
                    'product_uom_qty': 1.0,
                })
            else:
                # Create sale.order.line with our product and name
                sol_id = self.env['sale.order.line'].create({
                    'product_id': self.transport_mode.product_id.id,
                    'name': self.transport_mode.name,
                    'order_id': self.order_id,
                    'tax_id': self.tax_ids,

                    'date_planned': self.doa_est,
                    'price_unit': self.price,
                    'product_uom': self.env.ref('product.product_uom_unit').id,
                    'product_uom_qty': 1.0,
                })
                self.line_order_id = sol_id

                # self.env['sale.order'].browse(self.sale_order_id.id).write({
                #     'order_line': [ [4, sol_id ]]
                #     })

        return res


#########################
#  Transport Product Lines #
#########################
class DMProductLines(models.Model):
    _name = "dm.transport.order.line"

    transport_order_id = fields.Many2one(string="Transport order", comodel_name="dm.transport.order",
                                         ondelete="cascade", readonly=True)
    product_id = fields.Many2one(string="Product", comodel_name="product.product", domain=[('type', '!=', 'service')])
    weight = fields.Float(related="product_id.weight")
    volume = fields.Float(related="product_id.volume")
    qty = fields.Float(string="qty")
    weight_total = fields.Float(compute="_dummy")
    volume_total = fields.Float(compute="vt_compute")

    # Calculate weight and volume
    @api.depends('volume')
    @api.multi
    def vt_compute(self):
        for rec in self:
            qty = rec.qty
            rec.volume_total = rec.volume * qty
            rec.weight_total = rec.weight * qty

    @api.multi
    def _dummy(self):
        """
        Dummy function
        """
        return


#########################
#  Transport Order Mode #
#########################
class DMTransportOrderType(models.Model):
    _name = "dm.transport.mode"

    name = fields.Text(string="Name")
    product_id = fields.Many2one(string="Product", comodel_name='product.product', domain=[('type', '=', 'service')])
