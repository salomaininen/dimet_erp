from openerp import models, fields, api, _
import logging
# Logger for debug
_logger = logging.getLogger(__name__)


# -- Compose domain
def compose_domain(state):
    domain = "[('state','=','" + state + "')]"
    return domain


####################
# Business Process #
####################
class DMProcess(models.Model):
    _name = 'dm.process'
    _description = "Dimet Business Process"

    # Fields
    model_id = fields.Many2one(string="Odoo model", comodel_name='ir.model')
    name = fields.Char(string="Name", related='model_id.name')
    description = fields.Text(string="Description", translate=True)
    access_rules = fields.One2many(string="Access filters", comodel_name='dm.access.rule',
                                   inverse_name='process')

    ir_model_access = fields.One2many(string="Odoo Access rights", comodel_name='ir.model.access',
                                      inverse_name='model_id', related='model_id.access_ids')
    ir_rules = fields.One2many(string="Odoo Access rules", comodel_name='ir.rule',
                               inverse_name='model_id', related='model_id.access_rules')

    # SQL constraints
    _sql_constraints = [('dm_process_unique',
                         'UNIQUE (model_id)',
                         _('This business process already exists!'))]

# -- Create
    @api.model
    def create(self, vals):

        # Create new record first
        res = super(DMProcess, self).create(vals)

        # Create ir.rule records for access rules
        if not res.access_rules:
            return res

        model_id = res.model_id.id
        for access_rule in res.access_rules:
            # Compose domain
            domain = compose_domain(access_rule.state)
            rule_vals = {
                'domain_force': domain, 'model_id': model_id,
                'groups': [[6, False, [group.id for group in access_rule.groups]]],
                'name': 'dm_access_' + str(model_id),
                'perm_read': access_rule.perm_read if access_rule.perm_read else False,
                'perm_write': access_rule.perm_write if access_rule.perm_write else False,
                'perm_create': access_rule.perm_create if access_rule.perm_create else False,
                'perm_unlink': False
                # 'perm_unlink': access_rule.perm_unlink if access_rule.perm_unlink else False # TODO add if unlink will be implemented
            }

            # Create rule record
            new_rule = self.env['ir.rule'].sudo().create(rule_vals)
            access_rule.ir_rule = new_rule.id

        # Return
        self.env['ir.rule'].sudo().clear_cache()
        return res

# -- Create
    @api.multi
    def write(self, vals):
        _logger.info("Vals %s", vals)
        _logger.info("Context %s", self._context)
        # If model is vals change model delete all access rules
        new_model = True if "model_id" in vals else False

        # If access rules in vals redefine all ir rules for them
        redefine_rules = True if "access_rules" in vals else False

        # Write
        res = super(DMProcess, self).write(vals)

        # Return if no rules to redefine and model in vals
        if not redefine_rules:
            return res

        # Redefine rules
        for rec in self:
            _logger.info("Access rules %s", rec.access_rules)
            if rec.access_rules:
                for access_rule in rec.access_rules:
                    # Compose domain
                    domain = compose_domain(access_rule.state)
                    model_id = new_model if new_model else rec.model_id.id
                    rule_vals = {
                        'domain_force': domain, 'model_id': model_id,
                        'groups': [[6, False, [group.id for group in access_rule.groups]]],
                        'name': 'dm_access_' + str(model_id),
                        'perm_read': access_rule.perm_read if access_rule.perm_read else False,
                        'perm_write': access_rule.perm_write if access_rule.perm_write else False,
                        'perm_create': access_rule.perm_create if access_rule.perm_create else False,
                        'perm_unlink': False
                        # 'perm_unlink': access_rule.perm_unlink if access_rule.perm_unlink else False # TODO add if unlink will be implemented
                    }
                    # Create new rule in no rule exists..
                    if not access_rule.ir_rule:
                        new_rule = self.env['ir.rule'].sudo().create(rule_vals)
                        access_rule.ir_rule = new_rule.id
                    else:
                        access_rule.ir_rule.sudo().write(rule_vals)

        # Return
        self.env['ir.rule'].sudo().clear_cache()
        return res

# -- Unlink
    @api.multi
    def unlink(self):
        res = super(DMProcess, self).unlink()
        self.env['ir.rule'].sudo().clear_cache()
        return res

##################
# Sample Process #
##################
class DMBpSample(models.Model):
    _name = 'dm.bp.sample'
    _description = "Dimet Sample Business Process"

    # Fields
    name = fields.Char(string="Name")
    state = fields.Selection([
        ('0', 'Draft'),
        ('1', 'Confirmed'),
        ('a', 'Accepted'),
        ('p', 'In progress'),
        ('s', 'Suspended'),
        ('e', 'Complete'),
        ('d', 'Declined'),
        ('c', 'Cancelled'),
    ], string="State", required=True)
    note = fields.Text(string="Note")

