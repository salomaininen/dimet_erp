from openerp import models, fields, api, _, tools, SUPERUSER_ID
from datetime import datetime
# import logging
# Logger for debug
# _logger = logging.getLogger(__name__)


###########################
# Support ticket category #
###########################
class DMNewProduct(models.Model):
    _name = 'dm.bp.ticket.category'
    _description = "Support ticket category"

    # Fields
    name = fields.Char(string="Name", required=true)
    user_ids = fields.Many2many(string="Related users", comodel_name='res.users',
                                relation='dm_ticket_cat_users_rel',
                                column1='cat',
                                column2='uid')

    # SQL constraints
    _sql_constraints = [('ticket_cat_unique',
                         'UNIQUE (name)',
                         _('This ticket category already exists!'))]


######################
# Support ticket #
######################
class DMSupportTicket(models.Model):
    _name = 'dm.bp.ticket'
    _description = "Support ticket"
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    # Fields
    # Dates and priority
    date_deadline = fields.Date(string="Deadline", required=True, track_visibility='onchange')
    dt_confirmed = fields.Datetime(string="Confirmed", readonly=1)
    dt_accepted = fields.Datetime(string="Accepted", readonly=1)
    dt_work_started = fields.Datetime(string="Work started", readonly=1)
    dt_work_complete = fields.Datetime(string="Work complete", readonly=1)
    dt_done = fields.Datetime(string="All done", readonly=1)
    dt_cancelled = fields.Datetime(string="Cancelled", readonly=1)
    dt_declined = fields.Datetime(string="Declined", readonly=1)
    days_to_deadline = fields.Char(string="Days to deadline", compute="days_left")
    warn_level = fields.Integer(string="Warning level", compute="_dummy")

    # Gantt view
    gantt_start = fields.Date(string="Gantt start", compute="_get_gantt_start")
    gantt_end = fields.Date(string="Gantt end", compute="_get_gantt_end")
    gantt_progress = fields.Integer(string="Gantt progress", compute="_get_gantt_progress")

    # General data
    state = fields.Selection([
        ('0', 'Draft'),
        ('1', 'Confirmed'),
        ('a', 'Accepted'),
        ('o', 'To redo'),
        ('p', 'In progress'),
        ('s', 'Suspended'),
        ('e', 'Complete'),
        ('d', 'Done'),
        ('x', 'Declined'),
        ('c', 'Cancelled'),
    ], string="State", default='0', required=True, track_visibility='onchange')

    type = fields.Selection([
        ('b', 'Bug'),
        ('f', 'Feature'),
    ], string="Type", required=True)

    priority = fields.Selection([
        ('0', 'Low'),
        ('1', 'Regular'),
        ('2', 'High'),
    ], string="Priority", default='1', required=True, track_visibility='onchange')

    category_id = fields.Many2one(string="Category", comodel_name='dm.bp.ticket.category', required=True)
    name = fields.Char(string="Brief description", required=True, translate=True)
    note = fields.Text(string="Detailed description", translate=True)

    # Delegation
    assigned_id = fields.Many2one(string="Assigned to", comodel_name='res.users', track_visibility='onchange')

    # Messages
    message_notify = fields.Char(string="Message notification", compute='_message_notify')

    # Order
    _order = 'priority desc,id desc'

    # SQL constraints

    _track = {
        'state': {'dimet_erp.dm_ticket_state_changed': lambda self, cr, uid, obj, ctx=None: obj.state},
        'priority': {'dimet_erp.dm_ticket_priority_changed': lambda self, cr, uid, obj, ctx=None: obj.priority},
        'date_deadline': {'dimet_erp.dm_np_ticket_date_deadline_changed': lambda self, cr, uid, obj, ctx=None: obj.date_deadline},
        'assigned_id': {'dimet_erp.dm_ticket_assigned': lambda self, cr, uid, obj, ctx=None: obj.assigned_id}

    }

# -- Get gantt start
    @api.multi
    def _get_gantt_start(self):
        for rec in self:
            if rec.dt_confirmed:
                rec.gantt_start = rec.dt_confirmed
        return

# -- Get gantt end
    @api.multi
    def _get_gantt_end(self):
        for rec in self:
            if rec.state == 'd':
                rec.gantt_end = rec.dt_done
            elif rec.state == 'x':
                rec.gantt_end = rec.dt_declined
            elif rec.state == 'c':
                rec.gantt_end = rec.dt_cancelled

            else:
                date_now = fields.Date.today()
                deadline = rec.date_deadline
                if date_now > deadline:
                    rec.gantt_end = date_now
                else:
                    rec.gantt_end = deadline

# -- Get gantt progress
    @api.multi
    def _get_gantt_progress(self):
        for rec in self:
            if rec.state == 'a':
                rec.gantt_progress = 10
            elif rec.state == 'p':
                rec.gantt_progress = 55
            elif rec.state == 'e':
                rec.gantt_progress = 85
            elif rec.state in ['d', 'c', 'x']:
                rec.gantt_progress = 100
            else:
                rec.gantt_progress = 0


# -- Show message notification
    def _message_notify(self):
        for rec in self:
            rec.message_notify = "New!" if rec.message_unread else False

# -- Dummy function for computed fields
    @api.multi
    def _dummy(self):
        return

# -- Get days left until deadline and date miss
    @api.multi
    def days_left(self):
        date_now = datetime.today()
        date_format = tools.DEFAULT_SERVER_DATE_FORMAT
        for rec in self:
            days_left = (datetime.strptime(rec.date_deadline, date_format) - date_now).days
            days_to_deadline = str(days_left) if rec.state in ['a', 'p', 's'] else False
            rec.days_to_deadline = days_to_deadline
            if days_to_deadline:
                if days_left < 2:
                    rec.warn_level = 0
                elif days_left < 4:
                    rec.warn_level = 1


# -- Create
    @api.model
    def create(self, vals):

        # Create!
        res = super(DMNewProduct, self).create(vals)
        return res

# -- Write
    @api.multi
    def write(self, vals):

        # Subscribe person task assigned to if not already assigned
        if 'assigned_id' in vals and vals['assigned_id']:
            assigned_partner = self.env['res.users'].sudo().browse([vals['assigned_id']]).partner_id
            if assigned_partner:
                assigned_partner_id = assigned_partner.id
                for rec in self:
                    if assigned_partner_id not in rec.message_follower_ids.ids:
                        rec.message_subscribe(partner_ids=[assigned_partner_id])
        # Write!
        res = super(DMNewProduct, self).write(vals)
        return res

# - Confirm -
    @api.multi
    def button_confirm(self):
        return {
            'type': 'ir.actions.act_window.message',
            'title': _('Confirm your action'),
            'message': _('By pressing "YES" you will confirm this request. \n'
                         'Initial data cannot be modified beyond this point!'
                         '\n Use Messages and Internal Notes to add information and attachments'),
                'close_button_title': _("No"),
            'buttons': [{
                'type': 'method',
                'name': _('Yes'),
                'model': self._name,
                'method': 'act_confirm',
                'args': [self.ids]
            }]
        }

    @api.multi
    def act_confirm(self):
        self.ensure_one()
        self.write({'state': '1', 'dt_confirmed': fields.Datetime.now()})

# - Accept -
    @api.multi
    def button_accept(self):
        return {
            'type': 'ir.actions.act_window.message',
            'title': _('Confirm your action'),
            'message': _('By pressing "YES" you will accept this request'),
            'close_button_title': _("No"),
            'buttons': [{
                'type': 'method',
                'name': _('Yes'),
                'model': self._name,
                'method': 'act_accept',
                'args': [self.ids]
            }]
        }

    @api.multi
    def act_accept(self):
        self.ensure_one()
        self.write({'state': 'a', 'dt_accepted': fields.Datetime.now()})

# - Start works (progress) -
    @api.multi
    def button_progress(self):
        return {
            'type': 'ir.actions.act_window.message',
            'title': _('Confirm your action'),
            'message': _('By pressing "YES" you will start work on this request'),
            'close_button_title': _("No"),
            'buttons': [{
                'type': 'method',
                'name': _('Yes'),
                'model': self._name,
                'method': 'act_progress',
                'args': [self.ids]
            }]
        }

    @api.multi
    def act_progress(self):
        self.ensure_one()
        self.write({'state': 'p', 'dt_work_started': fields.Datetime.now()})

# - Suspend works -
    @api.multi
    def button_suspend(self):
        return {
            'type': 'ir.actions.act_window.message',
            'title': _('Confirm your action'),
            'message': _('By pressing "YES" you will suspend work on this request'),
            'close_button_title': _("No"),
            'buttons': [{
                'type': 'method',
                'name': _('Yes'),
                'model': self._name,
                'method': 'act_suspend',
                'args': [self.ids]
            }]
        }

    @api.multi
    def act_suspend(self):
        self.ensure_one()
        self.write({'state': 's'})


# - Finish works (complete) -
    @api.multi
    def button_complete(self):
        return {
            'type': 'ir.actions.act_window.message',
            'title': _('Confirm your action'),
            'message': _('By pressing "YES" you will finish work on this request'),
            'close_button_title': _("No"),
            'buttons': [{
                'type': 'method',
                'name': _('Yes'),
                'model': self._name,
                'method': 'act_complete',
                'args': [self.ids]
            }]
        }

    @api.multi
    def act_complete(self):
        self.ensure_one()
        self.write({'state': 'e', 'dt_work_complete': fields.Datetime.now()})


# - All done (done) -
    @api.multi
    def button_done(self):
        return {
            'type': 'ir.actions.act_window.message',
            'title': _('Confirm your action'),
            'message': _('By pressing "YES" you confirm thar all work on this request is done'),
            'close_button_title': _("No"),
            'buttons': [{
                'type': 'method',
                'name': _('Yes'),
                'model': self._name,
                'method': 'act_done',
                'args': [self.ids]
            }]
        }

    @api.multi
    def act_done(self):
        self.ensure_one()
        self.write({'state': 'd', 'dt_done': fields.Datetime.now()})

# - Cancel request (cancel) -
    @api.multi
    def button_cancel(self):
        return self.reason_wizard('c', _("Cancel request"))

# - Decline request -
    @api.multi
    def button_decline(self):
        return self.reason_wizard('x', _("Decline request"))

# -- To Redo
    @api.multi
    def button_to_redo(self):
        return self.reason_wizard('o', _("To redo"))


# -- Open reason wizard
    @api.multi
    def reason_wizard(self, action_id, reason=None):
        self.ensure_one()
        context = {
            'default_action_id': action_id,
            'model_name': 'dm.bp.ticket'
        }

        return {
            'name': reason,
            "views": [[False, "form"]],
            'res_model': 'dm.bp.sc.wiz',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': context
            }
