from openerp import models, fields, api, _
import logging
# Logger for debug
_logger = logging.getLogger(__name__)


#########
# Crane #
#########
class DMCrane(models.Model):
    _name = 'dm.crane'
    _inherit = 'dm.product.base'
    _description = 'Crane'

    # Fields
    # Tech specs
    capacity = fields.Integer(string="Capacity(kg)", required=True)
    span = fields.Integer(string="Span(mm)", required=True)
    working_group = fields.Selection([
        ('3', 'A3'),
        ('4', 'A4'),
        ('5', 'A5'),
        ('6', 'A6'),
        ('7', 'A7'),
        ('8', 'A8'),
    ], string="Working group(ISO)", required=True)
    trolley_line_ids = fields.One2many(string="Trolleys",
                                       comodel_name='dm.cargo.line', inverse_name='crane',
                                       auto_join=True)

    # Order
    _order = 'capacity, span, working_group'


#################
# Crane Trolley #
#################
class DMCraneTrolley(models.Model):
    _name = 'dm.crane.trolley'
    _inherit = 'dm.product.base'
    _description = 'Crane trolley'

    # Fields
    # Tech specs
    capacity = fields.Integer(string="Capacity(kg)", required=True)
    span = fields.Integer(string="Span(mm)", required=True)
    attachment_type = fields.Many2one(string="Attachment type", comodel_name='dm.machine.attachment.type',
                                      required=True)
    working_group = fields.Selection([
        ('1', 'M1'),
        ('2', 'M2'),
        ('3', 'M3'),
        ('4', 'M4'),
        ('5', 'M5'),
        ('6', 'M6'),
        ('7', 'M7'),
        ('8', 'M8'),
    ], string="Working group(ISO)", required=True)
    hoist_line_ids = fields.One2many(string="Trolleys",
                                       comodel_name='dm.cargo.line', inverse_name='crane',
                                       auto_join=True)

    # Order
    _order = 'capacity, span, working_group'
