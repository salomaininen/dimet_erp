from openerp import models, fields, api, _
import logging
# Logger for debug
_logger = logging.getLogger(__name__)


#############
# Reel #
#############
class DMReel(models.Model):
    _name = 'dm.reel'
    _description = 'Cable reel'

# -- Check if np id is required
    @api.multi
    def _get_np_required(self):
        params = self._context.get('params')
        if params:
            np_required = False if 'action' in params else True
        else:
            np_required = True
        return np_required

    # Fields
    parent_id = fields.Many2one(string="Parent reel", comodel_name='dm.reel')
    parent_variant = fields.Many2one(string="Parent reel variant", comodel_name='dm.reel.variant',
                                     domain="[('reel','=', parent_id)]")
    children = fields.One2many(string="Child reels", comodel_name='dm.reel', inverse_name='parent_id')
    children_count = fields.Integer(string="Children count", compute="count_children")
    # Product
    product = fields.Many2one(string="Related product", comodel_name='product.product')
    product_name = fields.Char(string="Name", related='product.name')
    # Name
    name = fields.Char(string="Name", compute='compose_name')
    name_draft = fields.Char(string="Name (no product)", translate=True)
    # Tech specs
    type = fields.Selection([
        ('s', 'Spring driven'),
        ('m', 'Motor driven'),
    ], default='s', string="Type", required=True)

    # Dimensions
    length = fields.Integer(string="Length(mm)", required=True)
    width = fields.Integer(string="Width(mm)", required=True)
    height = fields.Integer(string="Height(mm)", required=True)
    weight = fields.Integer(string="Weight(kg)", required=True)
    
    # Temperatures
    ip_class = fields.Many2one(string="IP protection class", comodel_name='dm.product.ip')
    temp_min = fields.Integer(string="Working temperature, min(C)", required=True)
    temp_max = fields.Integer(string="Working temperature, max(C)", required=True)

    # Power and capacity
    ring_count = fields.Integer(string="Ring count", required=True)
    amp = fields.Integer(string="Ring current(A)")
    power_1ph = fields.Float(string="Power 1 phase(kW)", digits=(6, 2),
                             help="Estimated power of attached equipment for 220V")
    power_3ph = fields.Float(string="Power 3 phases(kW)", digits=(6, 2),
                             help="Estimated power of attached equipment for 380V")
    cable_length = fields.Integer(string="Cable length", required=True)
    kw = fields.Float(string="Motor power(kW)", digits=(6, 2))
    speed = fields.Integer(string="Speed(m/min)")

    # Request for new product
    np_id = fields.Many2one(string="Product design request id", comodel_name='dm.bp.np')
    np_required = fields.Boolean(string="Specify product design request", compute="_dummy",
                                 default=_get_np_required)
    # Documents
    documents = fields.Many2many(string="Documents", comodel_name='ir.attachment',
                                 compute="get_documents",
                                 inverse="set_documents")
    # Appius integration
    appius_id = fields.Char(string="Appius ID")

    # Notes
    note = fields.Text(string="Note", translate=True)
    # Variants
    variants = fields.One2many(string="Variants", comodel_name='dm.reel.variant',
                               inverse_name='reel')
    variants_count = fields.Integer(string="Variants count", compute="count_variants")

    # Order
    _order = 'ring_count, cable_length'

    # SQL constraints
    _sql_constraints = [('reel_product_unique',
                         'UNIQUE (product)',
                         _('This reel already exists!'))]

# -- Get documents
    @api.multi
    def get_documents(self):
        for rec in self:
            rec.documents = self.env['ir.attachment'].search(['&', ("res_model", '=', 'dm.reel'),
                                                              ('res_id', '=', rec.id)])

# -- Set documents
    @api.multi
    def set_documents(self):
        for rec in self:
            for document in rec.documents:
                if not document.res_id:
                    document.write({'res_model': 'dm.reel', 'res_id': rec.id})

# -- Amp changed
    @api.onchange('amp')
    @api.multi
    def apm_change(self):
        for rec in self:
            rec.power_1ph = rec.amp * 0.220
            rec.power_3ph = rec.amp * 0.220 * 1.73


# -- Name compose -- #
    @api.multi
    def compose_name(self):
        for rec in self:
            rec.name = rec.product_name if rec.product else rec.name_draft

# -- Dummy
    def _dummy(self):
        return

# -- Create -- #
    @api.model
    def create(self, vals):
        # Check if created from Request for new product add ref to np_id
        np_id = False
        if 'np_id' in vals:
            np_id = vals['np_id']

        # If has cargo lines set reel
        has_lines = True if 'cargo_lines' in vals else False

        # Create
        rec = super(DMReel, self).create(vals)

        # Update lines
        if has_lines:
            for line in rec.cargo_lines:
                line.reel = rec.id

        # Create NP line
        if np_id:
            ref = rec._name + "," + str(rec.id)
            self.env['dm.bp.np.line'].create({'np_id': np_id, 'dm_product_ref': ref})

        return rec

# -- Compute amp -- #
    @api.depends('kw')
    @api.multi
    def _compute_amp(self):
        for rec in self:
            kw = rec.kw
            if kw > 0:
                rec.amp = kw * 1000 / 220
            else:
                rec.amp = 0

# -- Compute kw -- #
    @api.onchange('amp')
    @api.multi
    def _compute_kw(self):
        for rec in self:
            rec.kw = rec.amp * 220 / 1000 if rec.amp else 0


# -- Count variants
    @api.multi
    def count_variants(self):
        for rec in self:
            rec.variants_count = self.env['dm.reel.variant'].search_count([('reel', '=', rec.id)])

# -- Open variants
    @api.multi
    def open_variants(self):
        self.ensure_one()
        current_reel_id = self.id
        context = {
            'default_reel': current_reel_id
        }

        tree_view_id = self.env['ir.ui.view'].search([('name', '=', 'dm.reel.variant.tree')]).id

        # Compose view header
        header = _("Variants of ") + self.name

        return {
            'name': header,
            "views": [[tree_view_id, "tree"], [False, "form"]],
            'res_model': 'dm.reel.variant',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'view_id': tree_view_id,
            'context': context,
            'domain': [('reel', '=', current_reel_id)]
        }

# -- Count children
    @api.multi
    def count_children(self):
        for rec in self:
            rec.children_count = self.env['dm.reel'].search_count([('parent_id', '=', rec.id)])

# -- Open children
    @api.multi
    def open_children(self):
        self.ensure_one()
        current_reel_id = self.id
        context = {
            'default_parent_id': current_reel_id
        }

        tree_view_id = self.env['ir.ui.view'].search([('name', '=', 'dm.reel.tree')]).id

        # Compose view header
        header = _("Children of ") + self.name

        return {
            'name': header,
            "views": [[tree_view_id, "tree"], [False, "form"]],
            'res_model': 'dm.reel',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'view_id': tree_view_id,
            'context': context,
            'domain': [('parent_id', '=', current_reel_id)]
        }


#####################
# Reel variant #
#####################
class DMReelVariant(models.Model):
    _name = 'dm.reel.variant'
    _description = 'Cable reel variant'

# -- Check if np id is required
    @api.multi
    def _get_np_required(self):
        params = self._context.get('params')
        if params:
            np_required = False if 'action' in params else True
        else:
            np_required = True
        return np_required

    # Fields
    parent_id = fields.Many2one(string="Parent variant", comodel_name='dm.reel.variant',
                                domain="[('reel','=', reel)]")
    has_parent = fields.Boolean(string="No parent", compute='check_parent')
    # Reel
    reel = fields.Many2one(string="Reel", comodel_name='dm.reel', required=True)
    # Product
    product = fields.Many2one(string="Related product", comodel_name='product.product')
    product_name = fields.Char(string="Name", related='product.name')
    # Name
    name = fields.Char(string="Name", compute='compose_name')
    name_draft = fields.Char(string="Name (no product)")
    # Dimensions
    length = fields.Integer(string="Length(mm)", required=True)
    width = fields.Integer(string="Width(mm)", required=True)
    height = fields.Integer(string="Height(mm)", required=True)
    weight = fields.Integer(string="Weight(kg)", required=True)
    # Temperatures
    ip_class = fields.Many2one(string="IP protection class", comodel_name='dm.product.ip')
    temp_min = fields.Integer(string="Working temperature, min(C)", required=True)
    temp_max = fields.Integer(string="Working temperature, max(C)", required=True)
    # Request for new product
    np_id = fields.Many2one(string="Product design request id", comodel_name='dm.bp.np')
    np_required = fields.Boolean(string="Specify product design request", compute="_dummy",
                                 default=_get_np_required)
    # Documents
    documents = fields.Many2many(string="Documents", comodel_name='ir.attachment',
                                 compute="get_documents",
                                 inverse="set_documents")
    # Notes
    note = fields.Text(string="Note", translate=True)

    # Appius integration
    appius_id = fields.Char(string="Appius ID")

    # Order
    _order = 'id'

    # SQL constraints
    _sql_constraints = [('reel_variant_product_unique',
                         'UNIQUE (product)',
                         _('This reel variant_already exists!'))]

# -- Dummy
    def _dummy(self):
        return

# -- Create -- #
    @api.model
    def create(self, vals):
        # Check if created from Request for new product add ref to np_id
        np_id = False
        if 'np_id' in vals:
            np_id = vals['np_id']

        # Create
        rec = super(DMReelVariant, self).create(vals)

        # Create NP line
        if np_id:
            ref = rec._name + "," + str(rec.id)
            self.env['dm.bp.np.line'].create({'np_id': np_id, 'dm_product_ref': ref})

        return rec

# -- Check parent
    @api.multi
    def check_parent(self):
        has_parent = True if self._context.get('default_reel', False) else False
        for rec in self:
            rec.has_parent = has_parent


# -- Reel changed
    @api.onchange('reel')
    @api.multi
    def reel_change(self):
        self.ensure_one()
        self.name_draft = self.reel.name
        self.weight = self.reel.weight
        self.length = self.reel.length
        self.width = self.reel.width
        self.height = self.reel.height
        self.temp_max = self.reel.temp_max
        self.temp_min = self.reel.temp_min
        self.ip_class = self.reel.ip_class

        self.ip_class = self.reel.ip_class

# -- Get documents
    @api.multi
    def get_documents(self):
        for rec in self:
            rec.documents = self.env['ir.attachment'].search(['&', ("res_model", '=', 'dm.reel.variant'),
                                                              ('res_id', '=', rec.id)])

# -- Set documents
    @api.multi
    def set_documents(self):
        for rec in self:
            for document in rec.documents:
                if not document.res_id:
                    document.write({'res_model': 'dm.reel.variant', 'res_id': rec.id})

# -- Compose name -- #
    @api.multi
    def compose_name(self):
        for rec in self:
            rec.name = rec.product_name if rec.product else rec.name_draft


