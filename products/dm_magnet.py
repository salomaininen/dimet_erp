from openerp import models, fields, api, _
import logging
# Logger for debug
_logger = logging.getLogger(__name__)


#################
# Magnet series #
#################
class DMMagnetSeries(models.Model):
    _name = 'dm.magnet.series'
    _description = 'Magnet series'

    # Fields
    name = fields.Char(string="Name", required=True, translate=True)

    # Order
    _order = 'name'

    # SQL constraints
    _sql_constraints = [('magnet_operation_unique',
                         'UNIQUE (name)',
                         _('This magnet operation already exists!'))]


####################
# Magnet operation #
####################
class DMMagnetOperation(models.Model):
    _name = 'dm.magnet.operation'
    _description = 'Magnet operation'

    # Fields
    name = fields.Char(string="Name", required=True, translate=True)
    ed_min = fields.Integer(string="ED min(%)", required=True)

    # Order
    _order = 'name'

    # SQL constraints
    _sql_constraints = [('magnet_operation_unique',
                         'UNIQUE (name)',
                         _('This magnet operation already exists!'))]


##################
# Magnet adapter #
##################
class DMMagnetAdapter(models.Model):
    _name = 'dm.magnet.adapter'
    _description = 'Magnet adapter'

# -- Check if np id is required
    @api.multi
    def _get_np_required(self):
        params = self._context.get('params')
        if params:
            np_required = False if 'action' in params else True
        else:
            np_required = True
        return np_required

    # Fields
    # Product
    product = fields.Many2one(string="Related product", comodel_name='product.product')
    product_name = fields.Char(string="Name", related='product.name')
    # Name
    name = fields.Char(string="Name", compute='compose_name')
    name_draft = fields.Char(string="Name (no product)")
    # Tech specs
    weight = fields.Integer(string="Weight(kg)", required=True)
    attachment_type = fields.Many2one(string="Attachment type", comodel_name='dm.machine.attachment.type', required=True)
    magnets = fields.Many2many(comodel_name='dm.magnet',
                               relation='magnet_adapter',
                               column1='adapter',
                               column2='magnet',
                               string="Compatible magnets", required=True)
    # Temperatures
    temp_min = fields.Integer(string="Working temperature, min(C)", required=True)
    temp_max = fields.Integer(string="Working temperature, max(C)", required=True)
    # Request for new product
    np_id = fields.Many2one(string="Product design request id", comodel_name='dm.bp.np')
    np_required = fields.Boolean(string="Specify product design request", compute="_dummy",
                                 default=_get_np_required)
    # Documents
    documents = fields.Many2many(string="Documents", comodel_name='ir.attachment',
                                 compute="get_documents", inverse="set_documents")
    # Appius integration
    appius_id = fields.Char(string="Appius ID")

    # Notes
    note = fields.Text(string="Note", translate=True)

    # -- Order
    _order = 'product'

# -- Dummy
    def _dummy(self):
        return

# -- Get documents
    @api.multi
    def get_documents(self):
        for rec in self:
            rec.documents = self.env['ir.attachment'].search(['&', ("res_model", '=', 'dm.magnet.adapter'),
                                                              ('res_id', '=', rec.id)])

# -- Set documents
    @api.multi
    def set_documents(self):
        for rec in self:
            for document in rec.documents:
                if not document.res_id:
                    document.write({'res_model': 'dm.magnet.adapter', 'res_id': rec.id})

    # SQL constraints
    _sql_constraints = [('magnet_adapter_product_unique',
                         'UNIQUE (product)',
                         _('This magnet adapter already exists!'))]


# -- Get the lightest magnet adapter for given attachment-magnet combination. Returns dict of id and weight of adapter
    @api.multi
    def get_adapter(self, attachment_id, magnet_id):
        # Get attachment type
        attachment_type = self.env['dm.machine.attachment'].search([('id','=',attachment_id)])[0].type.id
        # Get adapter
        adapter = self.search(['&',('attachment_type','=',attachment_type),('magnets','=',magnet_id)], order='weight asc')
        if adapter:
            return {
                'id': adapter[0].id,
                'name': adapter[0].name,
                'weight': adapter[0].weight
            }
        else:
            return False

# -- Name compose -- #
    @api.multi
    def compose_name(self):
        for rec in self:
            rec.name = rec.product_name if rec.product else rec.name_draft

# -- Create -- #
    @api.model
    def create(self, vals):
        # Check if created from Request for new product add ref to np_id
        if 'np_id' in vals:
            np_id = vals['np_id'] if vals['np_id'] else False

        # Create
        rec = super(DMMagnetAdapter, self).create(vals)

        # Create NP line
        ref = rec._name + "," + str(rec.id)
        self.env['dm.bp.np.line'].create({'np_id': np_id, 'dm_product_ref': ref})
        return rec


##########
# Magnet #
##########
class DMMagnet(models.Model):
    _name = 'dm.magnet'
    _description = 'Magnet'

# -- Check if np id is required
    @api.multi
    def _get_np_required(self):
        params = self._context.get('params')
        if params:
            np_required = False if 'action' in params else True
        else:
            np_required = True
        return np_required

    # Fields
    parent_id = fields.Many2one(string="Parent magnet", comodel_name='dm.magnet')
    parent_variant = fields.Many2one(string="Parent magnet variant", comodel_name='dm.magnet.variant',
                                     domain="[('magnet','=', parent_id)]")
    children = fields.One2many(string="Child magnets", comodel_name='dm.magnet', inverse_name='parent_id')
    children_count = fields.Integer(string="Children count", compute="count_children")
    # Product
    product = fields.Many2one(string="Related product", comodel_name='product.product')
    product_name = fields.Char(string="Name", related='product.name')
    # Name
    name = fields.Char(string="Name", compute='compose_name')
    name_draft = fields.Char(string="Name (no product)", translate=True)
    # Tech specs
    series = fields.Many2one(string="Series", comodel_name='dm.magnet.series', required=True)
    shape = fields.Selection([
        ('r', 'Round'),
        ('t', 'Rectangular'),
        ('s', 'Square'),
        ('c', 'Custom'),
    ], default='r', string="Shape", required=True)
    coil_material = fields.Selection([
        ('a', 'Aluminium'),
        ('c', 'Copper'),
        ('p', 'Permanent'),
    ], default='a', string="Coil material", required=True)
    length = fields.Integer(string="Length/diameter(mm)", required=True,
                            help='For round magnet is diameter')
    width = fields.Integer(string="Width(mm)", required=True)
    height = fields.Integer(string="Height(mm)", required=True)
    weight = fields.Integer(string="Weight(kg)", required=True)
    ed = fields.Integer(string="ED(%)", required=True)
    # Temperatures
    ip_class = fields.Many2one(string="IP protection class", comodel_name='dm.product.ip')
    temp_min = fields.Integer(string="Working temperature, min(C)", required=True)
    temp_max = fields.Integer(string="Working temperature, max(C)", required=True)
    # Power and capacity
    power_drop = fields.Integer(string="Power drop(% of max)", required=True)
    capacity_drop = fields.Integer(string="Capacity drop(% of max)", required=True)
    amp_cold = fields.Float(string="Current, cold (Amp)", digits=(6,2),
                            compute='_compute_amp_cold', inverse='_compute_kw_cold')
    amp_hot = fields.Float(string="Current, hot (Amp)", digits=(6,2),
                           compute='_compute_amp_hot', inverse='_compute_kw_hot')
    kw_cold = fields.Float(string="Power, cold (kW)", digits=(6,2), required=True)
    kw_hot = fields.Float(string="Power, hot (kW)", digits=(6,2), required=True)
    cargo_lines = fields.One2many(string="Lifting capacity",
                                  comodel_name='dm.cargo.line', inverse_name='magnet',
                                  auto_join=True)
    adapters = fields.Many2many(comodel_name='dm.magnet.adapter',
                                relation='magnet_adapter',
                                column2='adapter',
                                column1='magnet',
                                string="Compatible adapters",
                                help="Adapters for magnets to use with various attachments")
    # Request for new product
    np_id = fields.Many2one(string="Product design request id", comodel_name='dm.bp.np')
    np_required = fields.Boolean(string="Specify product design request", compute="_dummy",
                                 default=_get_np_required)
    # Documents
    documents = fields.Many2many(string="Documents", comodel_name='ir.attachment',
                                 compute="get_documents", inverse="set_documents")
    # Appius integration
    appius_id = fields.Char(string="Appius ID")

    # Notes
    note = fields.Text(string="Note", translate=True)
    # Variants
    variants = fields.One2many(string="Variants", comodel_name='dm.magnet.variant',
                               inverse_name='magnet')
    variants_count = fields.Integer(string="Variants count", compute="count_variants")

    # Order
    _order = 'shape, length, kw_cold'

    # SQL constraints
    _sql_constraints = [('magnet_product_unique',
                         'UNIQUE (product)',
                         _('This magnet already exists!'))]

# -- Get documents
    @api.multi
    def get_documents(self):
        for rec in self:
            rec.documents = self.env['ir.attachment'].search(['&', ("res_model", '=', 'dm.magnet'),
                                                              ('res_id', '=', rec.id)])

# -- Set documents
    @api.multi
    def set_documents(self):
        for rec in self:
            for document in rec.documents:
                if not document.res_id:
                    document.write({'res_model': 'dm.magnet', 'res_id': rec.id})

# -- Name compose -- #
    @api.multi
    def compose_name(self):
        for rec in self:
            rec.name = rec.product_name if rec.product else rec.name_draft

# -- Get existing ids -- # TODO fix it!

    @api.multi
    def _get_existing_ids(self):
        for rec in self:
            existing_products = self.env['dm.magnet'].search([('id', '>', 0)])
            ids = [product.id for product in existing_products]
            # rec.existing_products = ids
            # _logger.info("Existing ids %s", rec.existing_products)

# -- Dummy
    def _dummy(self):
        return

# -- Create -- #
    @api.model
    def create(self, vals):
        # Check if created from Request for new product add ref to np_id
        np_id = False
        if 'np_id' in vals:
            np_id = vals['np_id']

        # If has cargo lines set magnet
        has_lines = True if 'cargo_lines' in vals else False

        # Create
        rec = super(DMMagnet, self).create(vals)

        # Update lines
        if has_lines:
            for line in rec.cargo_lines:
                line.magnet = rec.id

        # Create NP line
        if np_id:
            ref = rec._name + "," + str(rec.id)
            self.env['dm.bp.np.line'].create({'np_id': np_id, 'dm_product_ref': ref})

        return rec

# -- Power drop changed-- #
    @api.onchange('power_drop')
    @api.multi
    def _power_drop_changed(self):
        for rec in self:
            multiplier = (100 - float(rec.power_drop)) / 100
            rec.kw_hot = rec.kw_cold * multiplier

# -- Capacity drop changed-- #
    @api.onchange('capacity_drop')
    @api.multi
    def _power_drop_changed(self):
        for rec in self:
            for line in rec.cargo_lines:
                line.calculate_capacity_effective()

# -- Compute amp cold -- #
    @api.depends('kw_cold')
    @api.multi
    def _compute_amp_cold(self):
        for rec in self:
            kw_cold = rec.kw_cold
            # kw_hot = rec.kw_hot
            if kw_cold > 0:
                rec.amp_cold = kw_cold * 1000 / 220
                # rec.power_drop = 100 - (round((kw_hot / kw_cold) * 100))
                # Calculate using power drop
                rec.kw_hot = kw_cold * (100-rec.power_drop)/100
            else:
                rec.amp_cold = 0

# -- Compute amp hot -- #
    @api.depends('kw_hot')
    @api.multi
    def _compute_amp_hot(self):
        for rec in self:
            kw_cold = rec.kw_cold
            kw_hot = rec.kw_hot
            if kw_hot: # and (kw_cold > 0):
                rec.amp_hot = kw_hot * 1000 / 220
                #  rec.power_drop = 100 - (round((kw_hot / kw_cold) * 100))
            else:
                rec.amp_hot = 0

# -- Compute kw cold -- #
    @api.onchange('amp_cold')
    @api.multi
    def _compute_kw_cold(self):
        for rec in self:
            rec.kw_cold = rec.amp_cold * 220 / 1000 if rec.amp_cold else 0

# -- Compute kw hot -- #
    @api.onchange('amp_hot')
    @api.multi
    def _compute_kw_hot(self):
        for rec in self:
            rec.kw_hot = rec.amp_hot * 220 / 1000 if rec.amp_hot else 0


# -- Count variants
    @api.multi
    def count_variants(self):
        for rec in self:
            rec.variants_count = self.env['dm.magnet.variant'].search_count([('magnet', '=', rec.id)])

# -- Open variants
    @api.multi
    def open_variants(self):
        self.ensure_one()
        current_magnet_id = self.id
        context = {
            'default_magnet': current_magnet_id
        }

        tree_view_id = self.env['ir.ui.view'].search([('name', '=', 'dm.magnet.variant.tree')]).id

        # Compose view header
        header = _("Variants of ") + self.name

        return {
            'name': header,
            "views": [[tree_view_id, "tree"], [False, "form"]],
            'res_model': 'dm.magnet.variant',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'view_id': tree_view_id,
            'context': context,
            'domain': [('magnet', '=', current_magnet_id)]
        }

# -- Count children
    @api.multi
    def count_children(self):
        for rec in self:
            rec.children_count = self.env['dm.magnet'].search_count([('parent_id', '=', rec.id)])

# -- Open children
    @api.multi
    def open_children(self):
        self.ensure_one()
        current_magnet_id = self.id
        context = {
            'default_parent_id': current_magnet_id
        }

        tree_view_id = self.env['ir.ui.view'].search([('name', '=', 'dm.magnet.tree')]).id

        # Compose view header
        header = _("Children of ") + self.name

        return {
            'name': header,
            "views": [[tree_view_id, "tree"], [False, "form"]],
            'res_model': 'dm.magnet',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'view_id': tree_view_id,
            'context': context,
            'domain': [('parent_id', '=', current_magnet_id)]
        }


##################
# Magnet variant #
##################
class DMMagnetVariant(models.Model):
    _name = 'dm.magnet.variant'
    _description = 'Magnet variant'

# -- Check if np id is required
    @api.multi
    def _get_np_required(self):
        params = self._context.get('params')
        if params:
            np_required = False if 'action' in params else True
        else:
            np_required = True
        return np_required

    # Fields
    parent_id = fields.Many2one(string="Parent variant", comodel_name='dm.magnet.variant',
                                domain="[('magnet','=', magnet)]")
    has_parent = fields.Boolean(string="No parent", compute='check_parent')
    # Magnet
    magnet = fields.Many2one(string="Magnet", comodel_name='dm.magnet', required=True)
    shape = fields.Selection(string="Shape", related='magnet.shape', readonly=True)
    # Product
    product = fields.Many2one(string="Related product", comodel_name='product.product')
    product_name = fields.Char(string="Name", related='product.name')
    # Name
    name = fields.Char(string="Name", compute='compose_name')
    name_draft = fields.Char(string="Name (no product)")
    length = fields.Integer(string="Length/diameter(mm)", required=True,
                            help='For round magnet is diameter')
    width = fields.Integer(string="Width(mm)", required=True)
    height = fields.Integer(string="Height(mm)", required=True)
    weight = fields.Integer(string="Weight(kg)", required=True)
    # Temperatures
    ip_class = fields.Many2one(string="IP protection class", comodel_name='dm.product.ip')
    temp_min = fields.Integer(string="Working temperature, min(C)", required=True)
    temp_max = fields.Integer(string="Working temperature, max(C)", required=True)
    # Request for new product
    np_id = fields.Many2one(string="Product design request id", comodel_name='dm.bp.np')
    np_required = fields.Boolean(string="Specify product design request", compute="_dummy",
                                 default=_get_np_required)
    # Documents
    documents = fields.Many2many(string="Documents", comodel_name='ir.attachment',
                                 compute="get_documents",
                                 inverse="set_documents")
    # Appius integration
    appius_id = fields.Char(string="Appius ID")

    # Notes
    note = fields.Text(string="Note", translate=True)

    # Order
    _order = 'id'

    # SQL constraints
    _sql_constraints = [('magnet_variant_product_unique',
                         'UNIQUE (product)',
                         _('This magnet variant_already exists!'))]

# -- Dummy
    def _dummy(self):
        return

# -- Create -- #
    @api.model
    def create(self, vals):
        # Check if created from Request for new product add ref to np_id
        np_id = False
        if 'np_id' in vals:
            np_id = vals['np_id']

        # Create
        rec = super(DMMagnetVariant, self).create(vals)

        # Create NP line
        if np_id:
            ref = rec._name + "," + str(rec.id)
            self.env['dm.bp.np.line'].create({'np_id': np_id, 'dm_product_ref': ref})

        return rec

# -- Check parent
    @api.multi
    def check_parent(self):
        has_parent = True if self._context.get('default_magnet', False) else False
        for rec in self:
            rec.has_parent = has_parent


# -- Magnet changed
    @api.onchange('magnet')
    @api.multi
    def magnet_change(self):
        self.ensure_one()
        self.name_draft = self.magnet.name
        self.weight = self.magnet.weight
        self.length = self.magnet.length
        self.width = self.magnet.width
        self.height = self.magnet.height
        self.temp_max = self.magnet.temp_max
        self.temp_min = self.magnet.temp_min
        self.ip_class = self.magnet.ip_class

# -- Get documents
    @api.multi
    def get_documents(self):
        for rec in self:
            rec.documents = self.env['ir.attachment'].search(['&', ("res_model", '=', 'dm.magnet.variant'),
                                                              ('res_id', '=', rec.id)])

# -- Set documents
    @api.multi
    def set_documents(self):
        for rec in self:
            for document in rec.documents:
                if not document.res_id:
                    document.write({'res_model': 'dm.magnet.variant', 'res_id': rec.id})

# -- Compose name -- #
    @api.multi
    def compose_name(self):
        for rec in self:
            rec.name = rec.product_name if rec.product else rec.name_draft


###########################
# Magnet selection wizard #
###########################
class DMMagnetSelection(models.TransientModel):
    _name = 'dm.magnet.selection'

    capacity_max = fields.Integer(string="Maximum lifting capacity(kg)", required=True)
    capacity_real = fields.Integer(string="Real lifting capacity(kg)", compute='_get_ec',
                                   help="= (Maximum capacity - capacity reservation)")
    capacity_gap = fields.Integer(string="Capacity reservation(% of max capacity)",
                                  help="% of max capacity deducted")
    capacity_overload = fields.Integer(string="Capacity overload (% of max)")
    power_gap = fields.Integer(string="Power reservation(% of max power)",
                               help="% of max power deducted")
    ed_min = fields.Integer(string="ED min(%)")

    power_max = fields.Integer(string="Max power(kW)")
    power_effective = fields.Integer(string="Effective power(kW)", compute='_get_ep',
                                     help="= (Max power - Capacity power reservation)")
    # Cargo
    cargo = fields.Many2one(string="Cargo", comodel_name='dm.cargo', required=True)
    cargo_name = fields.Char(string="Cargo name", related='cargo.name')
    cargo_density = fields.Integer(string="Cargo density(kg/m3)", readonly=True)
    # Operation
    magnet_operation = fields.Many2one(string="Operation", comodel_name='dm.magnet.operation', required=True)
    # Material handler selection
    machine_brand = fields.Many2one(string='Brand', comodel_name='dm.machine.brand')
    material_handler = fields.Many2one(string='Model', comodel_name='dm.material.handler',
                                       domain="[('brand', '=', machine_brand)]")
    material_handler_variant = fields.Many2one(string='Variant', comodel_name='dm.material.handler.variant',
                                               domain="[('material_handler', '=', material_handler)]")
    attachments = fields.Many2many(string="Attachments", comodel_name='dm.machine.attachment',
                                   domain="[('mh_variants', '=', material_handler_variant)]")
    attachments_weight = fields.Integer(string="Attachment weight(kg)")

# -- Magnet operation change -- #
    @api.onchange('magnet_operation')
    @api.multi
    def _magnet_operation_change(self):
        self.ensure_one()
        self.ed_min = self.magnet_operation.ed_min if self.magnet_operation else 0

# -- Compute attachments weight -- #
    @api.onchange('attachments')
    @api.multi
    def _change_attachments(self):
        self.ensure_one()
        # Compute attachments weight
        att_weight = 0
        for attachment in self.attachments:
            att_weight += attachment.weight
        self.attachments_weight = att_weight

# -- Compute capacity based on machine and attachments -- #
    @api.onchange('material_handler_variant', 'attachments_weight')
    @api.multi
    def _change_machine(self):
        self.ensure_one()
        # Compute attachments weight
        capacity_left = self.material_handler_variant.capacity_min - self.attachments_weight if self.material_handler_variant else 0
        self.capacity_max = capacity_left


# -- Compute volume  -- #
    @api.onchange('cargo')
    @api.multi
    def _change_cargo(self):
        self.ensure_one()
        self.cargo_density = self.cargo.density

# -- Compute real capacity -- #
    @api.depends('capacity_max', 'capacity_gap')
    @api.multi
    def _get_ec(self):
        for rec in self:
            rec.capacity_real = round(rec.capacity_max * (100-rec.capacity_gap)/100)

# -- Compute efficient power -- #
    @api.depends('power_max', 'power_gap')
    @api.multi
    def _get_ep(self):
        for rec in self:
            rec.power_effective = round(rec.power_max * (100-rec.power_gap)/100)


# - Search magnet that fit required criteria -
    @api.multi
    def button_search(self):
        self.ensure_one()
        capacity_real = self.capacity_real
        power_effective = self.power_effective

        # Check if attachments are filled in and select first one if yes
        attachment_id = self.attachments[0].id if self.attachments else None

        # Search all magnets that fits criteria
        res = self.select_magnets(capacity_real, self.cargo.id, self.magnet_operation.id, attachment_id)
        # TODO: show warning if nothing matches selected criteria
        if not res:
            return
        # {
        #        'type': 'ir.actions.act_window.message',
        #        'title': _('Magnet selection'),
        #        'message': _('No magnet matches your search criteria!')
        #    }

        lines_ok = []
        for magnet in res['magnets']:
            lines_ok.append(magnet['cargo_line_id'])

        context = {
            'capacity_limit': capacity_real,
            'power_effective': power_effective
        }
        context.update(res)

        # _logger.info("Res: %s", res)
        # Select view depending on adapter presence
        tree_view_name = 'dm.cargo.line.tree.machines.attachment' if res['has_adapter'] > 0 else 'dm.cargo.line.tree.machines'
        tree_view_id = self.env['ir.ui.view'].search([('name', '=', tree_view_name)]).id

        # Generate view with cargo lines ids and context. Compose view header
        caption_middle = self.material_handler_variant.complete_name if self.material_handler_variant else str(capacity_real) + _("kg capacity")
        if power_effective > 0:
            caption_middle += " (" + str(power_effective) + "kW max)"
        header = _("Result for ") + caption_middle + _("  ") + self.magnet_operation.name + " " + self.cargo_name
        return {
            'name': header,
            "views": [[tree_view_id, "tree"], [False, "form"]],
            'res_model': 'dm.cargo.line',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'view_id': tree_view_id,
            'context': context,
            'domain': [('id', 'in', lines_ok)]
        }


# --------------------------------------------
# -- Select magnet that fits required criteria
# --------------------------------------------
    @api.multi
    def select_magnets(self, capacity, cargo_id, operation_id, attachment_id=None):
        # Get magnet ED
        ed_min = self.env['dm.magnet.operation'].browse([operation_id]).ed_min
        # Step #1. Find all cargo lines with magnets that weight less than maximum capacity (Pmax)
        cargo_lines = self.env['dm.cargo.line'].search(
            ['&', '&', ('cargo', '=', cargo_id),
             ('magnet_weight', '<=', capacity),
             ('magnet_ed', '>=', ed_min)]).sorted(key=lambda r: r.capacity_complete)

        # Compose list of ids of magnets with (magnet_weight + magnet_capacity_max) <= capacity_max
        result = {
            'has_adapter': 0,
            'magnets': []
        }
        rec_count = 0

        # If has attachment
        if attachment_id:
            # Check if adapter is needed
            type_id = self.env['dm.machine.attachment'].browse([attachment_id]).type.id
            has_adapter = self.env['dm.magnet.adapter'].search_count([('attachment_type', '=', type_id)])
            # If adapter required
            if has_adapter > 0:
                result['has_adapter'] = 1
                for cargo_line in cargo_lines:
                    res = self.env['dm.magnet.adapter'].get_adapter(attachment_id, cargo_line.magnet.id)
                    if not res:
                        continue
                    if capacity >= (cargo_line.magnet_weight + cargo_line.capacity_max + res['weight']):
                        result['magnets'].append({
                            'cargo_line_id': cargo_line.id,
                            'adapter_id': res['id'],
                            'adapter_weight': res['weight'],
                            'adapter_name': res['name']
                        })
                        rec_count += 1

            # If no adapter is needed
            else:
                for cargo_line in cargo_lines:
                    if capacity >= (cargo_line.magnet_weight + cargo_line.capacity_max):
                        result['magnets'].append({'cargo_line_id': cargo_line.id})
                        rec_count += 1

        # If no attachment
        else:
            for cargo_line in cargo_lines:
                if capacity >= (cargo_line.magnet_weight + cargo_line.capacity_max):
                    result['magnets'].append({'cargo_line_id': cargo_line.id})
                    rec_count += 1

        # Return dict 'Result' or False if nothing is selected
        if rec_count == 0:
            return False
        else:
            return result
