from openerp import models, fields, api, _
import logging
# Logger for debug
_logger = logging.getLogger(__name__)


#############
# Separator #
#############
class DMSeparator(models.Model):
    _name = 'dm.separator'
    _description = 'Separator'

# -- Check if np id is required
    @api.multi
    def _get_np_required(self):
        params = self._context.get('params')
        if params:
            np_required = False if 'action' in params else True
        else:
            np_required = True
        return np_required

    # Fields
    parent_id = fields.Many2one(string="Parent separator", comodel_name='dm.separator')
    parent_variant = fields.Many2one(string="Parent separator variant", comodel_name='dm.separator.variant',
                                     domain="[('separator','=', parent_id)]")
    children = fields.One2many(string="Child separators", comodel_name='dm.separator', inverse_name='parent_id')
    children_count = fields.Integer(string="Children count", compute="count_children")
    # Product
    product = fields.Many2one(string="Related product", comodel_name='product.product')
    product_name = fields.Char(string="Name", related='product.name')
    # Name
    name = fields.Char(string="Name", compute='compose_name')
    name_draft = fields.Char(string="Name (no product)", translate=True)
    # Tech specs
    type = fields.Selection([
        ('s', 'Suspended overband'),
        ('c', 'Self-cleaning'),
        ('d', 'Drum'),
    ], default='s', string="Type", required=True)
    drive_power = fields.Float(string="Drive power(kW)", digits=(6, 2))

    coil_material = fields.Selection([
        ('a', 'Aluminium'),
        ('c', 'Copper'),
        ('p', 'Permanent'),
    ], default='a', string="Coil material", required=True)

    # Separation specs
    conveyor_belt_width = fields.Integer(string="Conveyor belt width(mm)")
    conveyor_belt_speed = fields.Integer(string="Conveyor belt speed(m/min)")
    separation_depth = fields.Integer(string="Separation depth(mm)")
    inclusion_weight_min = fields.Integer(string="Inclusion weight,min(gram)")
    inclusion_weight_max = fields.Integer(string="Inclusion weight,max(gram)")

    # Dimensions
    length = fields.Integer(string="Length/diameter(mm)", required=True,
                            help='For round separator is diameter')
    width = fields.Integer(string="Width(mm)", required=True)
    height = fields.Integer(string="Height(mm)", required=True)
    weight = fields.Integer(string="Weight(kg)", required=True)
    
    # Temperatures
    ip_class = fields.Many2one(string="IP protection class", comodel_name='dm.product.ip')
    temp_min = fields.Integer(string="Working temperature, min(C)", required=True)
    temp_max = fields.Integer(string="Working temperature, max(C)", required=True)
    coolant_type = fields.Selection([
        ('n', 'Natural'),
        ('o', 'Oil'),        
    ], default='n', string="Coolant type", required=True)
    
    # Power and capacity
    amp = fields.Float(string="Current, cold (Amp)", digits=(6, 2),
                       compute='_compute_amp', inverse='_compute_kw')
    
    kw = fields.Float(string="Power, cold (kW)", digits=(6, 2), required=True)
    #cargo_lines = fields.One2many(string="Lifting capacity",
    #                              comodel_name='dm.cargo.separator', inverse_name='separator',
    #                              auto_join=True)

    # Request for new product
    np_id = fields.Many2one(string="Product design request id", comodel_name='dm.bp.np')
    np_required = fields.Boolean(string="Specify product design request", compute="_dummy",
                                 default=_get_np_required)
    # Documents
    documents = fields.Many2many(string="Documents", comodel_name='ir.attachment',
                                 compute="get_documents",
                                 inverse="set_documents")
    # Appius integration
    appius_id = fields.Char(string="Appius ID")

    # Notes
    note = fields.Text(string="Note", translate=True)
    # Variants
    variants = fields.One2many(string="Variants", comodel_name='dm.separator.variant',
                               inverse_name='separator')
    variants_count = fields.Integer(string="Variants count", compute="count_variants")

    # Order
    _order = 'type, kw'

    # SQL constraints
    _sql_constraints = [('separator_product_unique',
                         'UNIQUE (product)',
                         _('This separator already exists!'))]

# -- Get documents
    @api.multi
    def get_documents(self):
        for rec in self:
            rec.documents = self.env['ir.attachment'].search(['&', ("res_model", '=', 'dm.separator'),
                                                              ('res_id', '=', rec.id)])

# -- Set documents
    @api.multi
    def set_documents(self):
        for rec in self:
            for document in rec.documents:
                if not document.res_id:
                    document.write({'res_model': 'dm.separator', 'res_id': rec.id})

# -- Name compose -- #
    @api.multi
    def compose_name(self):
        for rec in self:
            rec.name = rec.product_name if rec.product else rec.name_draft

# -- Dummy
    def _dummy(self):
        return

# -- Create -- #
    @api.model
    def create(self, vals):
        # Check if created from Request for new product add ref to np_id
        np_id = False
        if 'np_id' in vals:
            np_id = vals['np_id']

        # If has cargo lines set separator
        has_lines = True if 'cargo_lines' in vals else False

        # Create
        rec = super(DMSeparator, self).create(vals)

        # Update lines
        if has_lines:
            for line in rec.cargo_lines:
                line.separator = rec.id

        # Create NP line
        if np_id:
            ref = rec._name + "," + str(rec.id)
            self.env['dm.bp.np.line'].create({'np_id': np_id, 'dm_product_ref': ref})

        return rec

# -- Compute amp -- #
    @api.depends('kw')
    @api.multi
    def _compute_amp(self):
        for rec in self:
            kw = rec.kw
            if kw > 0:
                rec.amp = kw * 1000 / 220
            else:
                rec.amp = 0

# -- Compute kw -- #
    @api.onchange('amp')
    @api.multi
    def _compute_kw(self):
        for rec in self:
            rec.kw = rec.amp * 220 / 1000 if rec.amp else 0


# -- Count variants
    @api.multi
    def count_variants(self):
        for rec in self:
            rec.variants_count = self.env['dm.separator.variant'].search_count([('separator', '=', rec.id)])

# -- Open variants
    @api.multi
    def open_variants(self):
        self.ensure_one()
        current_separator_id = self.id
        context = {
            'default_separator': current_separator_id
        }

        tree_view_id = self.env['ir.ui.view'].search([('name', '=', 'dm.separator.variant.tree')]).id

        # Compose view header
        header = _("Variants of ") + self.name

        return {
            'name': header,
            "views": [[tree_view_id, "tree"], [False, "form"]],
            'res_model': 'dm.separator.variant',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'view_id': tree_view_id,
            'context': context,
            'domain': [('separator', '=', current_separator_id)]
        }

# -- Count children
    @api.multi
    def count_children(self):
        for rec in self:
            rec.children_count = self.env['dm.separator'].search_count([('parent_id', '=', rec.id)])

# -- Open children
    @api.multi
    def open_children(self):
        self.ensure_one()
        current_separator_id = self.id
        context = {
            'default_parent_id': current_separator_id
        }

        tree_view_id = self.env['ir.ui.view'].search([('name', '=', 'dm.separator.tree')]).id

        # Compose view header
        header = _("Children of ") + self.name

        return {
            'name': header,
            "views": [[tree_view_id, "tree"], [False, "form"]],
            'res_model': 'dm.separator',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'view_id': tree_view_id,
            'context': context,
            'domain': [('parent_id', '=', current_separator_id)]
        }


#####################
# Separator variant #
#####################
class DMSeparatorVariant(models.Model):
    _name = 'dm.separator.variant'
    _description = 'Separator variant'

# -- Check if np id is required
    @api.multi
    def _get_np_required(self):
        params = self._context.get('params')
        if params:
            np_required = False if 'action' in params else True
        else:
            np_required = True
        return np_required

    # Fields
    parent_id = fields.Many2one(string="Parent variant", comodel_name='dm.separator.variant',
                                domain="[('separator','=', separator)]")
    has_parent = fields.Boolean(string="No parent", compute='check_parent')
    # Separator
    separator = fields.Many2one(string="Separator", comodel_name='dm.separator', required=True)
    # Product
    product = fields.Many2one(string="Related product", comodel_name='product.product')
    product_name = fields.Char(string="Name", related='product.name')
    # Name
    name = fields.Char(string="Name", compute='compose_name')
    name_draft = fields.Char(string="Name (no product)")
    # Dimensions
    length = fields.Integer(string="Length/diameter(mm)", required=True,
                            help='For round separator is diameter')
    width = fields.Integer(string="Width(mm)", required=True)
    height = fields.Integer(string="Height(mm)", required=True)
    weight = fields.Integer(string="Weight(kg)", required=True)
    # Temperatures
    ip_class = fields.Many2one(string="IP protection class", comodel_name='dm.product.ip')
    temp_min = fields.Integer(string="Working temperature, min(C)", required=True)
    temp_max = fields.Integer(string="Working temperature, max(C)", required=True)
    # Request for new product
    np_id = fields.Many2one(string="Product design request id", comodel_name='dm.bp.np')
    np_required = fields.Boolean(string="Specify product design request", compute="_dummy",
                                 default=_get_np_required)
    # Documents
    documents = fields.Many2many(string="Documents", comodel_name='ir.attachment',
                                 compute="get_documents",
                                 inverse="set_documents")
    # Notes
    note = fields.Text(string="Note", translate=True)

    # Appius integration
    appius_id = fields.Char(string="Appius ID")

    # Order
    _order = 'id'

    # SQL constraints
    _sql_constraints = [('separator_variant_product_unique',
                         'UNIQUE (product)',
                         _('This separator variant_already exists!'))]

# -- Dummy
    def _dummy(self):
        return

# -- Create -- #
    @api.model
    def create(self, vals):
        # Check if created from Request for new product add ref to np_id
        np_id = False
        if 'np_id' in vals:
            np_id = vals['np_id']

        # Create
        rec = super(DMSeparatorVariant, self).create(vals)

        # Create NP line
        if np_id:
            ref = rec._name + "," + str(rec.id)
            self.env['dm.bp.np.line'].create({'np_id': np_id, 'dm_product_ref': ref})

        return rec

# -- Check parent
    @api.multi
    def check_parent(self):
        has_parent = True if self._context.get('default_separator', False) else False
        for rec in self:
            rec.has_parent = has_parent


# -- Separator changed
    @api.onchange('separator')
    @api.multi
    def separator_change(self):
        self.ensure_one()
        self.name_draft = self.separator.name
        self.weight = self.separator.weight
        self.length = self.separator.length
        self.width = self.separator.width
        self.height = self.separator.height
        self.temp_max = self.separator.temp_max
        self.temp_min = self.separator.temp_min
        self.ip_class = self.separator.ip_class

# -- Get documents
    @api.multi
    def get_documents(self):
        for rec in self:
            rec.documents = self.env['ir.attachment'].search(['&', ("res_model", '=', 'dm.separator.variant'),
                                                              ('res_id', '=', rec.id)])

# -- Set documents
    @api.multi
    def set_documents(self):
        for rec in self:
            for document in rec.documents:
                if not document.res_id:
                    document.write({'res_model': 'dm.separator.variant', 'res_id': rec.id})

# -- Compose name -- #
    @api.multi
    def compose_name(self):
        for rec in self:
            rec.name = rec.product_name if rec.product else rec.name_draft


