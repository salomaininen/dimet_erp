from openerp import models, fields, api, tools
try:
    import cStringIO as StringIO
except ImportError:
    import StringIO

from .. dm_tools import dm_image

class DMCompany(models.Model):
    _inherit = "res.company"

    @api.multi
    @api.depends("seal")
    def _get_image(self):
         """ calculate the images sizes and set the images to the corresponding
         fields
         """
         for rec in self:
            image = rec.seal
            size = (rec.size_seal_width, rec.size_seal_high)
            if image:
               # check if the context contains the magic `bin_size` key
               if self.env.context.get("bin_size"):
                   # refetch the image with a clean context
                   image = self.env[self._name].with_context({}).browse(rec.id).seal
               try:
                   data = dm_image.image_get_resized_images(image, size)
                   rec.seal_medium = data["image_small"]
               except Exception:
                  pass

    # Fields
    pricelist_default = fields.Many2one(string="Default pricelist", comodel_name='product.pricelist',
                                        required=True)
    invoice_auto = fields.Boolean(string="Auto create/validate invoice", default=False)
    use_ru_proforma = fields.Boolean(string="Use Russian pro-forma", default=False)
    seal = fields.Binary(string="Seal", required=False, help="Company seal image")
    seal_medium = fields.Binary(string="Seal 128x128", store=False,compute="_get_image",
                                 help="Company seal image 128x128")
    size_seal_width = fields.Integer(string="size of seal width",default=250)
    size_seal_high = fields.Integer(string="size of seal high",default=250)
    ceo_id = fields.Many2one(string="Company CEO", comodel_name='res.users', auto_join=True)
    ca_id = fields.Many2one(string="Company Chief Accountant", comodel_name='res.users', auto_join=True)
    ceo_name = fields.Char(compute='_ceo_name', compute_sudo=True, store=True)
    ca_name = fields.Char(compute='_ca_name', compute_sudo=True, store=True)
    ceo_signature_medium = fields.Binary(compute='_ceo_sign', compute_sudo=True, store=True)
    ca_signature_medium = fields.Binary(compute='_ca_sign', compute_sudo=True, store=True)


# -- CEO name
    @api.depends('ceo_id', 'ca_id')
    @api.multi
    def _ceo_name(self):
        for rec in self:
            rec.ceo_name = rec.ceo_id.name

# -- CA name
    @api.depends('ceo_id', 'ca_id')
    @api.multi
    def _ca_name(self):
        for rec in self:
            rec.ca_name = rec.ca_id.name

# -- CEO signature
    @api.depends('ceo_id', 'ca_id')
    @api.multi
    def _ceo_sign(self):
        for rec in self:
            rec.ceo_signature_medium = rec.ceo_id.dm_signature_medium

# -- CA signature
    @api.depends('ceo_id', 'ca_id')
    @api.multi
    def _ca_sign(self):
        for rec in self:
            rec.ca_signature_medium = rec.ca_id.dm_signature_medium
