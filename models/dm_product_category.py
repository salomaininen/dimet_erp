from openerp import models, fields, api, _
from openerp.addons import decimal_precision as dp
# import logging
# Logger for debug
# _logger = logging.getLogger(__name__)


#######################################
#  Product Category Template Variable #
#######################################
class DMProductTemplateCategoryVariable(models.Model):
    _name = "dm.prod.cat.tmpl.variable"

    # Fields
    name = fields.Char(string="Name", required=True, translate=True)
    type = fields.Selection([
        ('c', 'Char'),
        ('n', 'Numeric'),
    ], string="Type", required=True)
    rm_spaces = fields.Boolean(string="Remove whitespaces")

    # Order
    _order = 'name'


##############################################
#  Product Category Template Variable  Value #
##############################################
class DMProductTemplateCategoryVariableValue(models.Model):
    _name = "dm.prod.cat.tmpl.variable.value"

# -- Fields
    variable_id = fields.Many2one(string="Variable", comodel_name='dm.prod.cat.tmpl.variable', auto_join=True)
    variable_name = fields.Char(string="Name", related='variable_id.name', readonly=True)
    type = fields.Selection(string="Type", related='variable_id.type', readonly=True)
    name = fields.Char(string="Value", compute='compose_name')
    val_char = fields.Char(string="Char value", translate=True)
    val_float = fields.Float(string="Float value")
    rm_spaces = fields.Boolean(string="Remove whitespaces", related='variable_id.rm_spaces', readonly=True)

    # Order
    _order = 'val_char,val_float'

    # SQL constraints
    _sql_constraints = [('variable_id,val_char,val_float',
                         'UNIQUE (variable_id,val_char,val_float)',
                         _('Value must be unique!'))]

# - Model constraints
    @api.constrains('val_char', 'val_float')
    def check_vals(self):
        for rec in self:
            if rec.val_char:
                duplicate_count = self.env['dm.prod.cat.tmpl.variable.value'].sudo().search_count(['&', ('variable_id', '=', rec.variable_id.id), ('val_char', '=', rec.val_char)])
            else:
                duplicate_count = self.env['dm.prod.cat.tmpl.variable.value'].sudo().search_count(['&', ('variable_id', '=', rec.variable_id.id), ('val_float', '=', rec.val_float)])
            if duplicate_count > 1:
                raise ValueError("Such value already exists!")

# - Name search
    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        res = []
        if name.isdigit():
            val_float = float(name)
            records = self.search(['|', ('val_char', 'ilike', name), '&', ('val_float', '>=', val_float), ('val_float', '<', (val_float + 1))]
                                  + args, limit=limit)
        else:
            records = self.search([('val_char', 'ilike', name)] + args, limit=limit)
        if records:
            for record in records:
                res.append((record['id'], record['name']))
        return res

# - Compose name
    @api.multi
    def compose_name(self):
        for rec in self:
            rec.name = rec.val_char if rec.val_char else str(rec.val_float)

# - Char value changed
    @api.onchange('val_char', 'rm_spaces')
    @api.multi
    def val_char_change(self):
        for rec in self:
            if rec.val_char:
                if rec.variable_id.rm_spaces:
                    rec.val_char = rec.val_char.replace(" ", "")


###################################
#  Product Category Template line #
###################################
class DMProductTemplateCategoryLine(models.Model):
    _name = "dm.prod.cat.tmpl.line"

    # Fields
    sequence = fields.Integer(string="Sequence", required=True)
    product_category_id = fields.Many2one(string="Product Category", comodel_name='product.category',
                                          required=False)
    variable_id = fields.Many2one(string="Term", comodel_name='dm.prod.cat.tmpl.variable', auto_join=True)
    name = fields.Char(string="Name", related='variable_id.name', readonly=True)
    note = fields.Char(string="Description")
    type = fields.Selection(string="Type", related='variable_id.type', readonly=True)
    prefix = fields.Char(string="Prefix", translate=True)
    suffix = fields.Char(string="Suffix", translate=True)
    required = fields.Boolean(string="Required")
    rm_spaces = fields.Boolean(string="Remove whitespaces", related='variable_id.rm_spaces', readonly=True)

    # Order
    _order = 'product_category_id,sequence'


#####################
#  Product Category #
#####################
class DMProductCategory(models.Model):
    _inherit = "product.category"

    # Fields
    template_lines = fields.One2many(string="Template Lines", comodel_name='dm.prod.cat.tmpl.line',
                                     inverse_name='product_category_id')

# - Rename all products in category
    @api.multi
    def rename_products(self):
        for rec in self:
            products = self.env['product.template'].search([('categ_id', '=', rec.id)])
            for product in products:
                old_name = product.name
                product.compose_name()
                new_name = product.name
                # TODO  Log old name if product name changed
                if old_name != new_name:
                    product.message_post(body='Renamed %s into %s' % (old_name, new_name))


