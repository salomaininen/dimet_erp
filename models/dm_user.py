from openerp import models, fields, api, tools, _

try:
    import cStringIO as StringIO
except ImportError:
    import StringIO

from ..dm_tools import dm_image

import logging
# Logger for debug
_logger = logging.getLogger(__name__)


#########
#  User #
#########
class DMUser(models.Model):
    _inherit = "res.users"

    @api.multi
    @api.depends("dm_signature")
    def _get_image(self):
        """ calculate the images sizes and set the images to the corresponding
         fields
         """
        for rec in self:
            image = rec.dm_signature
            size = (rec.size_signature_width, rec.size_signature_high)
            if image:
                # check if the context contains the magic `bin_size` key
                if self.env.context.get("bin_size"):
                    # refetch the image with a clean context
                    image = self.env[self._name].with_context({}).browse(rec.id).dm_signature
                try:
                    data = dm_image.image_get_resized_images(image, size)
                    rec.dm_signature_medium = data["image_small"]
                except Exception:
                    pass

    # Fields
    default_section_id = fields.Many2one(required=False)
    sales_teams = fields.Many2many(string="Sales teams", comodel_name='crm.case.section',
                                   relation='sale_member_rel',
                                   column1='member_id',
                                   column2='section_id')
    crm_case_categ = fields.Many2many(string="CRM Claim categories", comodel_name='crm.case.categ',
                                      relation='dm_crm_case_categ_user',
                                      column1='user_id',
                                      column2='crm_case_categ')
    display_employees_suggestions = fields.Boolean(default=False)
    display_groups_suggestions = fields.Boolean(default=False)
    name = fields.Char(translate=True)
    dm_signature = fields.Binary(string="User's signature", required=False, help="User's signature image")
    dm_signature_medium = fields.Binary(string="User's signature 128x128", store=False, compute="_get_image",
                                        help="User's signature image 128x128")
    size_signature_width = fields.Integer(string="Signature width", default=250)
    size_signature_high = fields.Integer(string="Signature height", default=250)

# -- Indicators
    lead_ids = fields.One2many(string="Opportunities", comodel_name='crm.lead', inverse_name='user_id')
    sales_potential = fields.Float(string="Sales Potential", compute='get_sales_potential', store=True)
    sale_order_ids = fields.One2many(string="Sales Orders", comodel_name='sale.order', inverse_name='user_id')
    sales_balance = fields.Float(string="Sales Balance", compute='get_sales_balance', store=True)
    payment_planned_ids = fields.One2many(string="Scheduled payments", comodel_name='dm.payment.planned', inverse_name='user_id')
    scheduled_balance = fields.Float(string="Scheduled payments", compute='get_scheduled_balance', store=True)

# -- Get balance for scheduled payments
    @api.multi
    @api.depends('payment_planned_ids.amount_remaining', 'payment_planned_ids.state')
    def get_scheduled_balance(self):
        for rec in self:
            payments_planned = self.env['dm.payment.planned'].sudo().search(['&',
                                                                             ('user_id', '=', rec.id),
                                                                             ('state', '=', 's')])
            scheduled_balance = 0
            for payment_planned in payments_planned:
                scheduled_balance += payment_planned.amount_remaining
            rec.scheduled_balance = scheduled_balance

# -- Get sales balance
    @api.multi
    @api.depends('sale_order_ids.balance', 'sale_order_ids.state')
    def get_sales_balance(self):
        for rec in self:
            sales_orders = self.env['sale.order'].sudo().search(['&', '&',
                                                                 ('user_id', '=', rec.id),
                                                                 ('state', 'not in', ['draft', 'sent', 'cancel']),
                                                                 ('balance', '>', 0)])
            sales_balance = 0
            for sales_order in sales_orders:
                sales_balance += sales_order.balance
            rec.sales_balance = sales_balance

# -- Get sales potential
    @api.multi
    @api.depends('lead_ids.lead_potential')
    def get_sales_potential(self):
        for rec in self:
            opportunities = self.env['crm.lead'].sudo().search(['&', '&', '&',
                                                                ('type', '=', 'opportunity'),
                                                                ('user_id', '=', rec.id),
                                                                ('probability', '>', 0),
                                                                ('probability', '<', 100)])
            sales_potential = 0
            # _logger.info("User %s", rec.name)
            for opportunity in opportunities:
                # _logger.info("Opp id=%s name=%s sales potential=%s", opportunity.id, opportunity.name, opportunity.lead_potential)
                sales_potential += opportunity.lead_potential
            # _logger.info("Sales potential User %s =%s", rec.name, sales_potential)
            # sales_potential = sum(opportunity.lead_potential for opportunity in opportunities)
            rec.sales_potential = sales_potential

# -- Open Opportunities
    @api.multi
    def open_opportunities(self):
        self.ensure_one()
        current_id = self.id

        context = {
            'stage_type': 'opportunity',
            'default_type': 'opportunity',
            'default_user_id': current_id,
            'needaction_menu_ref': 'sale.menu_sale_quotations'
        }

        form_view_id = self.env.ref('sale_crm.crm_case_form_view_oppor').id

        return {
            "name": _('Opportunities'),
            "views": [[False, "kanban"], [False, "tree"], [form_view_id, "form"]],
            'res_model': 'crm.lead',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'context': context,
            'domain': ['&', ('type', '=', 'opportunity'), ('user_id', '=', current_id)]
        }

# -- Open Unpaid Sales Orders
    @api.multi
    def open_unpaid(self):
        self.ensure_one()
        current_id = self.id

        context = {'default_user_id': current_id}

        tree_view_id = self.env.ref('dimet_erp.dm_sales_order_tree_mod').id
        form_view_id = self.env.ref('dimet_erp.dm_sales_order_mod').id

        return {
            "name": _('Sales Orders'),
            "views": [[tree_view_id, "tree"], [form_view_id, "form"]],
            'res_model': 'sale.order',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'context': context,
            'domain': ['&', '&', ('balance', '>', '0'),  ('state', 'not in', ['draft', 'sent', 'cancel']), ('user_id', '=', current_id)]
        }

# -- Open Scheduled Payments
    @api.multi
    def open_scheduled(self):
        self.ensure_one()
        current_id = self.id

        context = {'default_user_id': current_id}

        return {
            "name": _('Scheduled Payments'),
            "views": [[False, "tree"], [False, "kanban"], [False, "form"]],
            'res_model': 'dm.payment.planned',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'context': context,
            'domain': ['&', ('state', '=', 's'), ('user_id', '=', current_id)]
        }
