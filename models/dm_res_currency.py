# -*- coding: utf-8 -*-
from openerp import models, fields, api, exceptions
import logging
import urllib2
import json
from datetime import datetime
# Logger for debug
_logger = logging.getLogger(__name__)

###############
#  res_currency #
###############
class DMResCurrency(models.Model):
    _inherit = "res.currency"

    cron_ref = fields.Reference(string='Setting update time (cron)', selection='get_cron_yahoo')
    base = fields.Boolean('Base')

    @api.model
    def get_cron_yahoo(self):
            names = self.env['ir.cron'].search([('name', 'in', ['Yahoo curency cron'])])
            cur =self.env["res.currency"].search([])
            for i in cur:
                i.cron_ref ="ir.cron,"+str(names[0].id)
            return [("ir.cron", "ir.cron")]

    @api.onchange('base')
    @api.multi
    def onchange_base(self):
        res = self.env['res.currency'].search_count([('base', '=', 1)])
        base_cur = self.env['res.currency'].search([('base', '=', 1)])
        res += int (self.base)
        if res > 1:
            self.base = 0
            res = {'warning': {
            'title': 'Warning',
            'message': "Onchange! Base currency must be only one! now base is: %s" % base_cur.name}}
        if res:
            return res

    @api.constrains('base')
    def _constrains_base(self):
        res = self.env['res.currency'].search_count([('base', '=', 1)])
        base_cur = self.env['res.currency'].search([('base', '=', 1)])
        res += int (self.base)
        if res > 1:
            self.base = False
            raise exceptions.ValidationError("Constrains! Base currency must be only one! now base is: %s" % base_cur.name)

    #NEW API use in button
    @api.multi
    def get_currency_yahoo(self):
        url_currency = ""
        base = self.env['res.currency'].sudo().search([('base', '=', True)])
        if len(base)>1:
            base = base[0]
        if base:
            for rec in self.env['res.currency'].sudo().search([]):
                url_currency += rec.name + base.name + "%2C"

            url_yahoo = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20(%22"+\
                        url_currency+\
                        "%22)&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback="
            response = urllib2.urlopen(url_yahoo)

            # Получаем весь html страницы
            html = response.read()
            jdata = json.loads(html)
            for rec in jdata["query"]["results"]["rate"]:
                    if rec["Name"] != "N/A" and rec["Rate"] != "N/A":
                        cur = self.env['res.currency'].sudo().search([('name', '=', rec["Name"].split("/")[0] )])
                        tol_id = self.env['res.currency.rate'].create({
                                     'name': datetime.today(),
                                     'currency_id': cur.id,
                                     'rate': rec["Rate"],  
                                 })        
            #self.rate_ids = self.env['res.currency.rate'].sudo().search([('currency_id', '=', self.id)])

    #OLD API for cron work
    def _get_currency_yahoo(self, cr, uid, context = None):
        url_currency = ""
        base_id = self.pool.get('res.currency').search(cr,uid,[('base', '=', True)])
        if len(base_id)>1:
            base_id = base_id[0]
        base = self.pool.get('res.currency').browse(cr, uid, base_id)
        currency_id = self.pool.get('res.currency').search(cr,uid,[])
        if base:
            for rec in currency_id:
                currency = self.pool.get('res.currency').browse(cr, uid, rec)
                url_currency += currency.name + base.name + "%2C"

            url_yahoo = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20(%22"+\
                        url_currency+\
                        "%22)&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback="
            response = urllib2.urlopen(url_yahoo)

            # Получаем весь html страницы
            html = response.read()
            jdata = json.loads(html)
            #for obj in currency_id:
            for rec in jdata["query"]["results"]["rate"]:
                    if rec["Name"] != "N/A" and rec["Rate"] != "N/A":
                        cur = self.pool.get('res.currency').search(cr,uid,[('name', '=', rec["Name"].split("/")[0] )])
                        tol_id = self.pool.get('res.currency.rate').create(cr,uid,{
                                     'name': datetime.today(),
                                     'currency_id': cur[0],
                                     'rate': rec["Rate"],  
                                 })
            #currency = self.pool.get('res.currency').browse(cr, uid, self.id)
            #currency.rate_ids = self.pool.get('res.currency.rate').search(cr,uid,[('currency_id', '=', self.id)])