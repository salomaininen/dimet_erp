from openerp import models, fields, api, _
from openerp.addons import decimal_precision as dp
import logging
# Logger for debug
_logger = logging.getLogger(__name__)

# Search models
SEARCH_MODELS = ['dm.magnet', 'dm.magnet.variant', 'dm.magnet.adapter', 'dm.grapple', 'dm.grapple.variant',
                 'dm.separator', 'dm.separator.variant']


########################
#  Pricelist Pricelist #
########################
class DMPricelist(models.Model):
    _inherit = "product.pricelist"

    # Fields
    is_public = fields.Boolean(string="Visible everywhere")
    company_ids = fields.Many2many(string="Companies", comodel_name='res.company',
                                   relation='dm_pricelist_company',
                                   column1='pricelist_id',
                                   column2='company_id')

    # Open pricelist versions
    @api.multi
    def open_items(self):
        self.ensure_one()
        ctx = {
            'default_pricelist_id': self.id
        }
        return {
            'name': _("Pricelist versions"),
            "views": [[False, "tree"], [False, "form"]],
            'res_model': 'product.pricelist.version',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain': [('pricelist_id', '=', self.id)],
            'context': ctx
        }


######################
#  Pricelist Version #
######################
class DMPricelistVersion(models.Model):
    _inherit = "product.pricelist.version"

    # Fields
    # company_id = fields.Many2one(required=True, readonly=False)

    # Open pricelist items
    @api.multi
    def open_items(self):
        self.ensure_one()
        ctx = {
            'default_price_version_id': self.id
        }
        return {
            'name': _("Pricelist items"),
            "views": [[False, "tree"], [False, "form"]],
            'res_model': 'product.pricelist.item',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain': [('price_version_id', '=', self.id)],
            'context': ctx
        }


######################
#  Product Name Line #
######################
class DMProductNameLine(models.Model):
    _name = "product.name.line"

# -- Fields
    product_tmpl_id = fields.Many2one(string="Product", comodel_name='product.template')
    cat_tmpl_id = fields.Many2one(string="Template line", comodel_name='dm.prod.cat.tmpl.line', auto_join=True)
    name = fields.Char(string="Name", compute='_compose_name', readonly=True)
    sequence = fields.Integer(string="Sequence", related='cat_tmpl_id.sequence', readonly=True)
    type = fields.Selection(string="Type", related='cat_tmpl_id.type', readonly=True)
    required = fields.Boolean(string="Required", related='cat_tmpl_id.required', readonly=True)
    variable_id = fields.Many2one(string="Variable", related='cat_tmpl_id.variable_id', readonly=True)
    value = fields.Many2one(string="Value", comodel_name='dm.prod.cat.tmpl.variable.value', required=False,
                            domain="[('variable_id', '=', variable_id)]",
                            auto_join=True)

    # SQL constraints
    _sql_constraints = [('pr_name_line_unique',
                         'UNIQUE (product_tmpl_id,cat_tmpl_id)',
                         _('Value must be unique!'))]

# -- Compose name
    @api.multi
    def _compose_name(self):
        for rec in self:
            rec.name = rec.cat_tmpl_id.name + '. ' + rec.cat_tmpl_id.note if rec.cat_tmpl_id.note else rec.cat_tmpl_id.name


#####################
#  Product Template #
#####################
class DMProductTemplate(models.Model):
    _inherit = "product.template"

# -- Default name lines.
    @api.multi
    def _default_name_lines(self):
        # _logger.info("Self = %s", self)
        self.ensure_one()
        self.categ_id_change()
        self.compose_name()

# - Fields
    type = fields.Selection(default='product')
    weight = fields.Float(required=True)
    weight_net = fields.Float(required=True)
    default_price = fields.Float(string="Default price", compute="_get_default_price",
                                 digits=dp.get_precision('Product Price'))
    lst_price = fields.Float(related='default_price')
    name_template_lines = fields.One2many(string="Name lines", comodel_name='product.name.line',
                                          inverse_name='product_tmpl_id', required=False, auto_join=True)
    # SQL constraints
    _sql_constraints = [('pr_tmpl_unique',
                         'UNIQUE (name)',
                         _('Name must be unique!'))]


# - Create
    @api.model
    def create(self, vals):
        # - Compose name
        # - If name lines reset name else exit
        if 'name_template_lines' in vals:
            if vals['name_template_lines'] and len(vals['name_template_lines']) > 0:
                vals['name'] = self.env['product.category'].browse(vals['categ_id']).name + ' '

        else:
            return super(DMProductTemplate, self).create(vals)

        for t_l_long in vals['name_template_lines']:
            t_l = t_l_long[2]
            if 'value' not in t_l:
                continue
            if not t_l['value']:
                continue
            res_line = ''

            # Get template id
            cat_tmpl_id = self.env['dm.prod.cat.tmpl.line'].browse(t_l['cat_tmpl_id'])

            # Prefix
            if cat_tmpl_id.prefix:
                res_line += cat_tmpl_id.prefix

            # Value
            res_line += self.env['dm.prod.cat.tmpl.variable.value'].browse(t_l['value']).name

            # Suffix
            if cat_tmpl_id.suffix:
                res_line += cat_tmpl_id.suffix

            # Append line to name
            vals['name'] += res_line

        return super(DMProductTemplate, self).create(vals)

# - Product category change
    @api.onchange('categ_id')
    @api.multi
    def categ_id_change(self):
        self.ensure_one()
        categ_template_lines = self.categ_id.template_lines
        if categ_template_lines:
            name_lines = []
            for line in categ_template_lines:
                name_line = {
                    'product_tmpl_id': self.id,
                    'cat_tmpl_id': line.id
                }
                name_lines += [name_line]
            self.update({'name_template_lines': name_lines})
        else:
            self.update({'name_template_lines': False})


# - Compose name from name template lines
    @api.onchange('name_template_lines')
    @api.multi
    def compose_name(self):
        # languages = self.env['res.lang'].browse()
        for rec in self:
            # Skip no template lines
            if len(rec.name_template_lines) < 1:
                continue

            # Loop through template lines
            res_name = rec.categ_id.name + ' '
            for t_l in rec.name_template_lines.sorted(key=lambda r: r.sequence):
                if not t_l.value:
                    continue
                res_line = ''

                cat_tmpl_id = t_l.cat_tmpl_id
                # Prefix
                if cat_tmpl_id.prefix:
                    res_line += cat_tmpl_id.prefix

                # Value
                res_line += t_l.value.name

                # Suffix
                if cat_tmpl_id.suffix:
                    res_line += cat_tmpl_id.suffix

                # Append line to name
                res_name += res_line

            # Set rec name
            if len(res_name) > 0:
                rec.name = res_name


# - Get list price
    @api.multi
    def _get_default_price(self):
        company_id = self.env.user.company_id.id
        pricelist_id = self.env['res.company'].browse([company_id]).pricelist_default.id
        if pricelist_id:
            for rec in self:
                res = self.pool.get('product.pricelist').price_get(self._cr, self._uid, [pricelist_id],
                                                                   rec.product_variant_ids[0].id, 1.0)
                rec.default_price = res[pricelist_id]

# - Open price wizard
    @api.multi
    def button_view_price(self):
        self.ensure_one()

        ctx = {
            'default_product': self.product_variant_ids[0].id
        }

        return {
            'name': _("Get price"),
            "views": [[False, "form"]],
            'res_model': 'dm.product.price.wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': ctx
        }

# - Open documents wizard
    @api.multi
    def button_show_documents(self):
        self.ensure_one()

        ctx = {
            'default_product_template': self.id
        }

        return {
            'name': _("Related attachments"),
            "views": [[False, "form"]],
            'res_model': 'dm.product.documents.wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': ctx
        }

# - Open product pricelist wizard
    @api.multi
    def button_show_pricelists(self):
        self.ensure_one()

        ctx = {
            'is_product_product': False
        }

        return {
            'name': _("Select pricelist"),
            "views": [[False, "form"]],
            'res_model': 'dm.product.pricelist.wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': ctx
        }


#####################
#  Product Product #
#####################
class DMProductProduct(models.Model):
    _inherit = "product.product"

# -- Fields
    # name_template_lines = fields.Many2one(related='product_tmpl_id.name_template_lines')
    list_price = fields.Float(compute="_get_list_price")
    standard_price = fields.Float(related='last_purchase_price')    # SQL constraints

    _sql_constraints = [('pr_pr_unique',
                         'UNIQUE (name)',
                         _('Name must be unique!'))]


# - Get list price
    @api.multi
    def _get_list_price(self):
        company_id = self.env.user.company_id.id
        pricelist_id = self.env['res.company'].browse([company_id]).pricelist_default.id
        if pricelist_id:
            for rec in self:
                res = self.pool.get('product.pricelist').price_get(self._cr, self._uid, [pricelist_id],
                                                                   rec.id, 1.0)
                rec.list_price = res[pricelist_id]

# - Open product pricelist wizard
    @api.multi
    def button_show_pricelists(self):
        self.ensure_one()

        ctx = {
            'is_product_product': True
        }

        return {
            'name': _("Select pricelist"),
            "views": [[False, "form"]],
            'res_model': 'dm.product.pricelist.wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': ctx
        }

# - Compose name from name template lines
    @api.onchange('name_template_lines')
    @api.multi
    def compose_name(self):
        # languages = self.env['res.lang'].browse()
        for rec in self:
            # Skip no template lines
            if len(rec.name_template_lines) < 1:
                continue

            # Loop through template lines
            res_name = rec.categ_id.name + ' '
            for t_l in rec.name_template_lines.sorted(key=lambda r: r.sequence):
                if not t_l.value:
                    continue
                res_line = ''

                cat_tmpl_id = t_l.cat_tmpl_id
                # Prefix
                if cat_tmpl_id.prefix:
                    res_line += cat_tmpl_id.prefix

                # Value
                res_line += t_l.value.name

                # Suffix
                if cat_tmpl_id.suffix:
                    res_line += cat_tmpl_id.suffix

                # Append line to name
                res_name += res_line

            # Set rec name
            if len(res_name) > 0:
                rec.name = res_name


# - Product category change
    @api.onchange('categ_id')
    @api.multi
    def categ_id_change(self):
        self.ensure_one()
        categ_template_lines = self.categ_id.template_lines
        if categ_template_lines:
            name_lines = []
            for line in categ_template_lines:
                name_line = {
                    'product_tmpl_id': self.id,
                    'cat_tmpl_id': line.id
                }
                name_lines += [name_line]
            self.update({'name_template_lines': name_lines})
        else:
            self.update({'name_template_lines': False})


#####################
# Price Show Wizard #
#####################
class DMPriceShowWizard(models.TransientModel):
    _name = 'dm.product.price.wizard'

    # Fields
    product = fields.Many2one(string="Product", comodel_name='product.product', required=True)
    qty = fields.Float(string="Quantity", default=1)
    company_id = fields.Many2one(string="Company", comodel_name='res.company',
                                 default=lambda self: self.env['res.users'].browse([self._uid]).company_id.id)

    pricelist = fields.Many2one(string="Pricelist", comodel_name='product.pricelist', required=True,
                                domain="['&',('company_id','=',company_id),('type','=','sale')]")
    partner = fields.Many2one(string="Customer", comodel_name='res.partner',
                              domain="[('customer','=',True)]")
    price = fields.Float(string="Price")

    @api.onchange('product_product', 'qty', 'pricelist', 'partner')
    @api.multi
    def get_price(self):
        self.ensure_one()
        pricelist_id = self.pricelist.id
        partner_id = self.partner.id if self.partner else False
        product_id = self.product.product_variant_ids[0].id
        if pricelist_id:
            res = self.pool.get('product.pricelist').price_get(self._cr, self._uid, [pricelist_id],
                                                               product_id, self.qty or 1.0, partner_id)
            self.price = res[pricelist_id]

    @api.onchange('partner')
    @api.multi
    def get_partner_pricelist(self):
        self.ensure_one()
        self.pricelist = self.partner.property_product_pricelist.id


#########################
# Documents Show Wizard #
#########################
class DMDocumentsShowWizard(models.TransientModel):
    _name = 'dm.product.documents.wizard'

    # Fields
    product_template = fields.Many2one(string="Product", comodel_name='product.template', required=True)
    documents = fields.Many2many(string="Documents", comodel_name='ir.attachment',
                                 compute="get_documents")

# -- Get related documents from DM Products
    @api.onchange('product_template')
    @api.multi
    def get_documents(self):
        for rec in self:
            # Get product documents first
            document_ids = self.env['ir.attachment'].search(
                ['&', ("res_model", '=', 'product.template'), ('res_id', '=', rec.product_template.id)]).ids

            # Search for related DM Products
            for model in SEARCH_MODELS:
                dm_product = self.env[model].search([("product", 'in', rec.product_template.product_variant_ids.ids)])
                if len(dm_product) > 0:
                    model_document_ids = self.env['ir.attachment'].search(
                        ['&', ("res_model", '=', model), ('res_id', 'in', dm_product.ids)]).ids
                    document_ids += model_document_ids
            rec.documents = self.env['ir.attachment'].browse(document_ids)


#################################
# Product show Pricelist wizard #
#################################
class DMProductPricelistWizard(models.TransientModel):
    _name = 'dm.product.pricelist.wizard'

    product_id = fields.Integer(string="Product ID", default=lambda self: self._context.get('active_id'))
    is_product_product = fields.Boolean(string="Is a product not template", default=lambda self: self._context.get('is_product_product'))
    product_presence = fields.Boolean(string="Product presence", default=False,
                                      help="Select what kind of pricelists to display: already having rules for this product or not yet")

    pricelist_id = fields.Many2one(string="Pricelist", comodel_name='product.pricelist', readonly=True)
    pricelist_version_id = fields.Many2one(string="Pricelist version", comodel_name='product.pricelist.version', required=True)

# -- Product presence option changed
    @api.onchange('product_presence')
    @api.multi
    def presence_change(self):
        self.ensure_one()

        search_domain = 'in' if self.product_presence else 'not in'
        result_ids = []  # Used to store ids of matching pricelist versions

        # Search pricelist items containing product
        if self.is_product_product:
            pricelist_items = self.env['product.pricelist.item'].search([('product_id', '=', self.product_id)])
        else:
            product_products = self.env['product.product'].search([('product_tmpl_id', '=', self.product_id)])
            pricelist_items = self.env['product.pricelist.item'].search([
                '|', ('product_tmpl_id', '=', self.product_id), ('product_id', 'in', product_products.ids)])

        for pricelist_item in pricelist_items:
            result_ids.append(pricelist_item.price_version_id.id)

        # Set domain for pricelist versions
        return {
            'domain': {'pricelist_version_id': [('id', search_domain, result_ids)]}
        }

# -- Pricelist version changed
    @api.multi
    @api.onchange('pricelist_version_id')
    def pricelist_version_change(self):
        self.ensure_one()
        self.pricelist_id = self.pricelist_version_id.pricelist_id.id

# -- Open items
    @api.multi
    def open_items(self):
        self.ensure_one()
        pricelist_version_id = self.pricelist_version_id.id
        if self.is_product_product:
            default_name = self.env['product.product'].browse([self.product_id]).name + ' (product)'
            ctx = {
                'default_price_version_id': pricelist_version_id,
                'default_product_id': self.product_id,
                'default_name': default_name
            }
        else:
            default_name = self.env['product.template'].browse([self.product_id]).name + ' (template)'
            ctx = {
                'default_price_version_id': pricelist_version_id,
                'default_product_tmpl_id': self.product_id,
                'default_name': default_name
            }

        return {
            'name': _("Pricelist items"),
            "views": [[False, "tree"], [False, "form"]],
            'res_model': 'product.pricelist.item',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain': [('id', '=', pricelist_version_id)],
            'context': ctx
        }
