# -*- coding: utf-8 -*-
from openerp import models, fields, api, _, tools
from openerp.exceptions import ValidationError
from datetime import datetime, timedelta
from pytils import numeral

# Logger for debug
import logging
_logger = logging.getLogger(__name__)


###############
#  Sales Team #
###############
class DMSaleTeam(models.Model):
    _inherit = "crm.case.section"

    # Fields
    company_id = fields.Many2one(string="Company", comodel_name='res.company',
                                 required=True)

    # -- override name_get
    @api.multi
    def name_get(self):
        res = []
        for rec in self:
            composed_name = rec.name
            res.append((rec.id, composed_name))
        return res


################
#  Sales Order #
################
class DMSaleOrder(models.Model):
    _inherit = "sale.order"

    @api.multi
    def get_note(self):
        self.ensure_one()
        return self.note if self.note else False

    # Fields
    expiration_date = fields.Date(string="Expiration date", track_visibility='onchange')
    validity_days = fields.Integer(string="Validity, days")
    validity_remaining = fields.Integer(string="Days before expiration", compute="days_left")
    opportunity = fields.Many2one(string="Opportunity/Lead", comodel_name='crm.lead')
    probability = fields.Float(string="Success Rate (%)", related='opportunity.probability', store=True, readonly=True)
    lead_manager = fields.Many2one(string="Lead manager", comodel_name='res.users', required=True)
    parent_for_invoice = fields.Boolean(string="Invoice to company", compute="get_parent_for_invoice",
                                        inverse="set_parent_for_invoice")
    parent_for_delivery = fields.Boolean(string="Deliver to company address", compute="get_parent_for_delivery",
                                         inverse="set_parent_for_delivery")
    parent_partner_id = fields.Many2one(string="Partner parent", related='partner_id.parent_id', readonly=True)
    # Price analysis
    discount_given = fields.Float(string="Discount, %", compute='_dummy')
    pricelist_total = fields.Float(string="Base amount", compute='get_discount_given')
    discount_total = fields.Float(string="Discount amount", compute='_dummy')
    # Russian pro-forma
    amount_total_char_ru = fields.Char(string="Amount total as Russian string", compute='_amount_total_str_ru')
    use_ru_proforma = fields.Boolean(string="Use Russian pro-forma", related='company_id.use_ru_proforma',
                                     readonly=True)
    schet_lines = fields.One2many('sale.order.schet.line', 'order_id', string="Sale order schet")
    schet_lines_count = fields.Integer(string="Schet line count", compute='_schet_line_count')
    schet_note = fields.Text(string="Comments")
    # Transport order
    # Relate to incoterm from sale.stock module
    incoterm_transport_required = fields.Boolean(string="Transport required", related='incoterm.transport_required',
                                                 readonly=True)
#    transport_orders = fields.One2many('dm.transport.order', 'sale_order_id', string="Transport orders")
    transport_order_count = fields.Integer(string="Transport line count", compute='_transport_order_count')

    # payments
    payment_ids = fields.One2many(string="Payments", comodel_name='account.move.line', compute='_get_payments')
    payments_exist = fields.Boolean(string="Payments exist", compute="_payments_exist")
    amount_paid = fields.Float(string="Paid Amount")
    amount_refunded = fields.Float(string="Refunded Amount")
    balance = fields.Float(string="Remaining Amount", compute='compute_balance', store=True)
    payment_planned_ids = fields.One2many(string="Planned payments", comodel_name='dm.payment.planned',
                                          inverse_name='order_id')
    payment_term = fields.Many2one(track_visibility="onchange")
    payment_term_fixed = fields.Boolean(related='payment_term.fixed', readonly=True)

# -- Distribute paid amount between payments
    """
    Distribute total paid amount between planned payments
    Change state of payments if paid completely
    """
    @api.multi
    def distribute_amount(self, amount_distribute=None):
        date_format = tools.DEFAULT_SERVER_DATE_FORMAT

        for sales_order in self:
            if not sales_order.payment_planned_ids:
                continue

            # Distribute amount among payments
            amount_left = amount_distribute or sales_order.amount_paid

            for rec in sales_order.payment_planned_ids.sorted(key=lambda x: x.payment_term):
                # Paid amount became smaller
                if rec.amount_paid > amount_left:
                    if rec.state == 'p':
                        if rec.date_triggered:
                            rec.write({'amount_paid': amount_left, 'state': 's'})
                        else:
                            rec.write({'amount_paid': amount_left, 'state': '1'})
                    else:
                        rec.write({'amount_paid': amount_left})

                # Paid amount became bigger
                elif rec.amount_paid < amount_left:
                    if rec.amount_total <= amount_left:
                        if not rec.state == 'p':
                            rec.write({'amount_paid': rec.amount_total,
                                       'state': 'p',
                                       'date_paid': datetime.now(),
                                       'days_overdue': (datetime.now()
                                                        - datetime.strptime(rec.date_calculated,
                                                                            date_format)).days if rec.date_calculated else 0})
                        else:
                            # Tmp logger
                            # _logger.info("Rec and Amount total %s ==> %s --> %s", rec.read(), rec.amount_total, amount_left)
                            rec.write({'amount_paid': rec.amount_total})
                    else:
                        # In case we missed some non confirmed payment
                        if rec.state == '0':
                            rec.write({'amount_paid': rec.amount_total, 'state': '1'})
                        else:
                            rec.write({'amount_paid': amount_left})

                # If nothing left to distribute set amount_left to zero
                delta = amount_left - rec.amount_total
                amount_left = 0 if delta < 0 else delta

# -- Check planned payment amount. Scan for both percent and amount
    @api.multi
    @api.constrains('payment_planned_ids')
    def _check_payment_planned(self):
        for rec in self:
            percent = 0.00
            amount_total = 0.00
            for line in rec.payment_planned_ids:
                percent += round(line.percent_computed, 2)
                amount_total += line.amount_total
            if not percent == 100:
                if 0 < rec.amount_total != amount_total:
                    raise ValidationError(_("100% of payments must be planned!"))

# -- Force cancel
    """ Force Sales Order to 'cancel' state in case it really needed """
    @api.multi
    def force_cancel(self):
        for rec in self:
            if not rec.invoice_ids:
                return
            for invoice in rec.invoice_ids:
                if not invoice.state == 'cancel':
                    return
            rec.state = 'cancel'

# -- Get refunded amount
    @api.multi
    def compute_refunded(self):
        for rec in self:
            refunded_total = 0
            for invoice in rec.invoice_ids:
                if invoice.type == 'out_refund' and invoice.state in ['open', 'paid']:
                    refunded_total += invoice.amount_total
            if refunded_total > 0:
                rec.amount_refunded = refunded_total

        # Compute paid
        self.compute_paid()

# -- Get remaining amount
    """ Paid amount = total credit for move line for out invoices + amount of out refunds"""
    @api.multi
    def compute_paid(self):
        for sales_order in self:
            amount_paid = 0
            for invoice in sales_order.invoice_ids:
                if invoice.type == 'out_invoice':
                    for payment in invoice.payment_ids:
                        amount_paid += payment.credit
            paid_total = amount_paid + sales_order.amount_refunded
            sales_order.amount_paid = paid_total

        # Distribute paid amount
        self.distribute_amount()

# -- Get balance
    @api.multi
    @api.depends('amount_paid')
    def compute_balance(self):
        for sales_order in self:
            sales_order.balance = sales_order.amount_total - sales_order.amount_paid

# -- Payments exist for Sales Order
    @api.multi
    def _payments_exist(self):
        for rec in self:
            rec.payments_exist = True if rec.payment_ids else False

    # -- Get all payments for Sales Order, Get payments for all invoices of Sales Order
    @api.multi
    def _get_payments(self):
        for sales_order in self:
            payments = []
            for invoice in sales_order.invoice_ids:
                payments_obj = invoice.payment_ids
                payment_ids = payments_obj.ids
                if payment_ids:
                    payments += payment_ids

            if payments:
                sales_order.update({
                    'payment_ids': payments,
                })
            # sales_order.payment_ids = payments


# -- Payment term change
    @api.onchange('payment_term')
    @api.multi
    def payment_term_change(self):
        self.ensure_one()
        # quit if no payment term lines in template
        if not self.payment_term.payment_planned_ids:
            return

        # Compose line
        self.payment_planned_ids = False
        term_lines = []
        for line in self.payment_term.payment_planned_ids:
            term_line = {
                'order_id': self.id,
                'state': '0',
                'percent': line.percent,
                'payment_term': line.payment_term,
                'days_term': line.days_term
            }
            term_lines += [term_line]

        self.update({'payment_planned_ids': term_lines})

# -- Validity change
    @api.onchange('validity_days')
    @api.multi
    def validity_days_change(self):
        self.ensure_one()
        date_format = tools.DEFAULT_SERVER_DATETIME_FORMAT
        date_order = datetime.strptime(self.date_order, date_format)
        self.expiration_date = date_order + timedelta(days=self.validity_days)

# -- Get validity days left
    @api.depends('expiration_date')
    @api.multi
    def days_left(self):
        date_format = tools.DEFAULT_SERVER_DATE_FORMAT
        date_now = datetime.today()
        for rec in self:
            if rec.expiration_date:
                days_left = (datetime.strptime(rec.expiration_date, date_format) - date_now).days
                rec.validity_remaining = days_left if rec.state in ['draft', 'sent'] else False

# -- Print Russian Pro-Forma
    @api.multi
    def print_schet(self):
        '''
        This function prints the sales order schet
        '''
        report_obj = self.pool.get('ir.actions.report.xml')
        qwebtypes = ['qweb-pdf', 'qweb-html']
        conditions = [('report_type', 'in', qwebtypes), ('report_name', '=', 'dimet_erp.report_schetorder')]
        idreport = report_obj.search(self._cr, self._uid, conditions)
        report = report_obj.browse(self._cr, self._uid, idreport[0])

        if report.attachment:
            for record_id in self._ids:
                # Select only schet attachment for delete, always have one schet attachment
                filename = ('Schet_' + self.pool[report.model].browse(self._cr, self._uid, record_id).name + '.pdf')

                # If the user has checked 'Reload from Attachment'
                if report.attachment_use:
                    alreadyindb = [('datas_fname', '=', filename),
                                   ('res_model', '=', report.model),
                                   ('res_id', '=', record_id)]
                    attach_ids = self.env['ir.attachment'].search(alreadyindb)

                    # Delete all attachments on record for reload attachment report, not load from database!
                    attach_ids.sudo().unlink()
                    # for i in attach_ids:
                    # self.env['ir.attachment'].browse(i).sudo().unlink()

        return self.env['report'].get_action(self.browse(self._ids), 'dimet_erp.report_schetorder')

# -- Get amount total as Russian string
    @api.depends('amount_total')
    @api.multi
    def _amount_total_str_ru(self):
        for rec in self:
            rubles, kops = divmod(rec.amount_total, 1)
            str_out = u'Итого: ' + numeral.in_words(int(rubles)) + u' руб. ' + numeral.in_words(
                int(kops * 100)) + u' коп.' \
                      + u', в том числе налоги: ' + str(rec.amount_tax) + u'руб.'
            rec.amount_total_char_ru = str_out

# -- Count schet lines
    @api.multi
    def _schet_line_count(self):
        for rec in self:
            rec.schet_lines_count = len(rec.schet_lines) if rec.schet_lines else 0

# -- Count transport orders
    @api.multi
    def _transport_order_count(self):
        res = 0
        for rec in self:
            cnt = self.env["dm.transport.order"].search_count([('order_id', '=', self.id), ('transport_type', '=', 'o')])
            rec.transport_order_count = cnt
            res += cnt
        return res


# -- View transport orders
    @api.multi
    def action_transport_view(self):
        """
        return view transport order tree
        """
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'view_id': False,
            'res_model': 'dm.transport.order',
            'nodestroy': True,
           # 'context': self._context,
            'res_id': False,
            'create': True,
            'domain': [('order_id','=',self._context['sale_order_id'])],
        }

    @api.depends('order_line')
    @api.multi
    def compute_price_subtotal(self):
        schet_model = self.pool.get('sale.order.schet.line')
        for rec in self:
            for line in rec.order_line:
                tax_id = False
                if len(line.tax_id):
                    tax_id = [[6, False, [i.id for i in line.tax_id]]]
                vals = {
                    'so_line_id': line.id,
                    'order_id': line.order_id.id,
                    'name': line.name,
                    'price_unit': line.price_unit,
                    'product_uom_qty': line.product_uom_qty,
                    'product_uom': line.product_uom.id,
                    'my_tax_ids': tax_id,
                    'discount': line.discount,
                    'product_id': line.product_id.id,
                    'so_line_add_ids': [],
                }
                # if object exist rewrite/update it else create
                schet_id = schet_model.search(self._cr, self._uid, [('so_line_id', '=', line.id)])
                if schet_id:
                    for so_line_add in schet_id:
                        schet_model.browse(self._cr, self._uid, so_line_add).unlink()
                    # schet_model.browse(self._cr, self._uid, schet_id).write(vals)
                    schet_model.create(self._cr, self._uid, vals, context=None)
                else:
                    schet_model.create(self._cr, self._uid, vals, context=None)
        return

# -- Dummy function
    @api.multi
    def _dummy(self):
        return

# -- Calculate discount given
    @api.depends('order_line')
    @api.multi
    def get_discount_given(self):
        for rec in self:
            amount_untaxed = rec.amount_untaxed
            if amount_untaxed <= 0: continue

            pricelist_total = discount_given = discount_total = sales_total = 0
            for line in rec.order_line:
                # Calculate only stockable products TODO check services we sell too
                if line.product_id.type == 'product':
                    if line.pricelist_price > 1:
                        product_uom_qty = line.product_uom_qty
                        pricelist_total += line.pricelist_price * product_uom_qty
                        sales_total += line.price_unit * product_uom_qty
                        discount_total += pricelist_total - sales_total
                        discount_given = 100 - (sales_total / pricelist_total) * 100 if pricelist_total > 0 else 0

            rec.pricelist_total = pricelist_total
            rec.discount_total = discount_total
            rec.discount_given = discount_given

# -- Get parent 4 invoice
    @api.depends('partner_id')
    @api.multi
    def get_parent_for_invoice(self):
        for rec in self:
            if rec.partner_id:
                if rec.partner_id.parent_id:
                    rec.parent_for_invoice = True if rec.partner_invoice_id.id == rec.partner_id.parent_id.id else False

# -- Get parent 4 delivery
    @api.depends('partner_id')
    @api.multi
    def get_parent_for_delivery(self):
        for rec in self:
            if rec.partner_id:
                if rec.partner_id.parent_id:
                    rec.parent_for_delivery = True if rec.partner_shipping_id.id == rec.partner_id.parent_id.id else False


# -- Set parent 4 invoice
    @api.multi
    @api.onchange('parent_for_invoice', 'partner_id')
    def set_parent_for_invoice(self):
        self.ensure_one()
        if self.partner_id:
            if self.parent_for_invoice:
                if self.partner_id.parent_id:
                    self.partner_invoice_id = self.partner_id.parent_id.id
            else:
                self.partner_invoice_id = self.partner_id.id

# -- Set parent 4 delivery
    @api.multi
    @api.onchange('parent_for_delivery', 'partner_id')
    def set_parent_for_delivery(self):
        self.ensure_one()
        if self.partner_id:
            if self.parent_for_delivery:
                if self.partner_id.parent_id:
                    self.partner_shipping_id = self.partner_id.parent_id.id
            else:
                self.partner_shipping_id = self.partner_id.id


# -- Set lead manager
    @api.onchange('opportunity')
    @api.multi
    def set_lead_manager(self):
        for rec in self:
            if rec.opportunity.lead_manager:
                rec.lead_manager = rec.opportunity.lead_manager.id
            else:
                is_lead_manager = self.env['res.users'].has_group('dimet_erp.dm_lead_manager')
                rec.lead_manager = self._uid if is_lead_manager else False

# -- Confirm sale
    @api.multi
    def action_button_confirm(self):

        # Check if lead manager is set
        for rec in self:
            if not rec.lead_manager:
                return {
                    'type': 'ir.actions.act_window.message',
                    'title': _('Lead manager not specified!'),
                    'message': _('Lead manager must be assigned before Sales Order is confirmed!'),
                    'close_button_title': _("Ok")
                }

        super(DMSaleOrder, self).action_button_confirm()
        for rec in self:
            if rec.company_id.invoice_auto:
                inv_id = rec.action_invoice_create()
                if inv_id:
                    inv = self.env['account.invoice'].browse(inv_id)
                    inv.sudo().signal_workflow('invoice_open')
            # Mark opportunity won if Sales Order from Opportunity
            if rec.opportunity:
                rec.opportunity.case_mark_won()


# -- Create
    @api.model
    def create(self, vals):
        # Add lead manager as follower
        if 'message_follower_ids' in vals:
            if 'lead_manager' in vals:
                if vals['lead_manager']:
                    if 'user_id' in vals:
                        if vals['lead_manager'] != vals['user_id']:
                            partner_id = self.env['res.users'].browse(vals['lead_manager']).partner_id.id
                            if vals['message_follower_ids']:
                                vals['message_follower_ids'].append([4, partner_id])
                            else:
                                vals['message_follower_ids'] = [[4, partner_id]]
                                # Create transport order
                                #       if self.incoterm_transport_required:
                                #      	self.env[]
                            #
                            #        _logger.info("Create vals %s", vals)
                            # if 'order_line' in vals:
                            #   for line in vals['order_line']:
                            # _logger.info("Product name is %s", line[2]['name'])
        res = super(DMSaleOrder, self).create(vals)

        # Recompute planned payments.
        """ Only the last payment line is computed by defult (bug?) """
        res.payment_planned_ids.compute_total()

        # Create transport order
        if 'incoterm' in vals:
            if self.env['stock.incoterms'].browse(vals['incoterm']).transport_required:
                # Create transport order
                self.env['dm.transport.order'].create({
                    'transport_type': 'o',
                    'name': res.name,
                    'user_id': self._uid,  
                    'order_id': res.id,
                    'source_ref': "sale.order,"+str(res.id),
                    'partner_shipping_id': vals['partner_shipping_id'],
                    'state': '0',
                    'message_follower_ids': res.message_follower_ids.ids,
                    })
        return res

# -- Write
    @api.multi
    def write(self, vals):

        # Get current and new order state
        state = vals.get('state', False)

        # Proceed parent function
        res = super(DMSaleOrder, self).write(vals)

        # Change state for Planned Payments
        if state in ['waiting_date', 'progress', 'manual']:
            payment_state = '1'
        elif state == 'cancel':
            payment_state = 'c'
        else:
            payment_state = False

        if payment_state:
            for rec in self:
                # Confirm only draft payments!
                for payment_line in rec.payment_planned_ids:
                    if payment_line.state == '0' and payment_state == '1':
                        payment_line.write({
                            'state': payment_state,
                            'message_follower_ids': rec.message_follower_ids.ids
                        })
                        # Trigger Order Confirmation
                        payment_line.trigger_term(['1', '5'])

                # But cancel all payments
                if payment_state == 'c':
                    rec.payment_planned_ids.write({'state': 'c'})

        # Set balance = amount total if state == manual
        # if vals.get('state', False) == 'manual':
        #    for rec in self:
        #        rec.balance = rec.amount_total

        if 'order_line' in vals:
            for line in vals["order_line"]:
                # We have line ==  [1, 125, {u'tax_id': [[6, False, [5]]]}]
                #     1-chang 0-create   ID   dict
                if line[2]:#just dict
                    if 'tax_id' in line[2]:
                        #in line [1]  there is ID of sle.order.line changed
                        tran_order = self.env['dm.transport.order'].search([
                            ('line_order_id', '=', line[1]) ,
                            ('order_id', '=', self.id)])
                        if tran_order:
                            #change in finded order tax_ids
                            tran_order.tax_ids = line[2]['tax_id']
        # delete from sale.order.schet.line items
        if 'schet_lines' in vals:
            for line in vals['schet_lines']:
                # _logger.info("Write vals LINE %s", line[2])
                if line[2]:
                    if 'so_line_add_ids' in line[2]:
                        for j in line[2]['so_line_add_ids']:
                            for i in j[2]:
                                # _logger.info("Write vals IN LINE IN LINE ID%s", i)
                                schet_model = self.pool.get('sale.order.schet.line')
                                schet_id = schet_model.search(self._cr, self._uid, [('so_line_id', '=', i)])
                                if schet_id:
                                    values = {u'schet_lines': [[2, schet_id[0], False]]}
                                    super(DMSaleOrder, self).write(values)
        # _logger.info("Write vals ORDER %s", vals)

        return res

    @api.multi
    def write_temp(self, vals):

        # Subscribe person task assigned to if not already assigned
        if 'lead_manager' in vals and vals['lead_manager']:
            lead_manager = self.env['res.users'].sudo().browse([vals['lead_manager']]).partner_id
            if lead_manager:
                lead_manager_id = lead_manager.id
                for rec in self:
                    if lead_manager_id not in rec.message_follower_ids.ids:
                        rec.message_subscribe(partner_ids=[lead_manager_id])

        return super(DMSaleOrder, self).write(vals)

    # -- Cancel order states MANUAL PROGRESS
    @api.multi
    def action_cancel(self):
        if super(DMSaleOrder, self).action_cancel():
            self.ensure_one()
            context = {
                'default_sales_order_id': self.id,
                'default_partner_id': self.partner_id.id,
                'default_salesman': self.user_id.id,
                'default_lead_manager': self.lead_manager.id,
            }

            return {
                'views': [[False, "form"]],
                'res_model': 'dm.opportunity.loss.wiz',
                'type': 'ir.actions.act_window',
                'target': 'new',
                'context': context,
            }


####################
#  Sale Order Line #
####################
class DMSaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    pricelist_price = fields.Float(string="Base price")
    probability = fields.Float(string="Success Rate (%)", related='order_id.probability', store=True, readonly=True)
    categ_id = fields.Many2one(string="Internal Category", related='product_id.categ_id', store=True, readonly=False)
    product_type = fields.Selection(related='product_id.type', readonly=True)
    discount_given = fields.Float(string="Discount, %", compute='_dummy')
    pricelist_subtotal = fields.Float(string="Subtotal, base", compute='get_discount_given')
    discount_subtotal = fields.Float(string="Discount, subtotal", compute='_dummy')
    move_ids = fields.One2many(
            comodel_name='stock.move',
            inverse_name='sale_line_id',
            string='Related Moves',
            readonly=True,
            ondelete='set null'
    )
    # Dates & durations
    date_planned = fields.Date(string="Planned date", required=False,
                               help="Planned delivery date")
    delay = fields.Integer(string="Days ready", help="Days for the goods to be ready or services to complete")
    delay_initial = fields.Integer(string="Initial delivery delay", readonly=True)

    date_order = fields.Date(string="Confirmation date", compute='_date_order', store=True)
    date_estimated = fields.Date(string="Estimated date", compute='_date_estimated',
                                 help="Estimated date when goods or services will be delivered")

    date_ready = fields.Date(string="Date ready", help="When the last item of the string was ready for delivery",
                             compute='get_date_delivered', inverse='_dummy', store=True)
    date_delivered = fields.Date(string="Date of delivery", help="Planned delivery date", compute='get_date_delivered',
                                 inverse='_dummy', store=True)
    date_accepted = fields.Date(string="Date of acceptance", help="Final acceptance date")
    days_delivered = fields.Integer(string="Days delivered", compute='_days_delivered', store=True)
    th_weight = fields.Float(string="Total weight", compute='_th_weight', store=True)

# -- Compute total weight
    @api.multi
    @api.depends('product_id', 'product_uom_qty', 'product_id.weight_net')
    def _th_weight(self):
        for rec in self:
            rec.th_weight = rec.product_id.weight * rec.product_uom_qty

    # -- Get days delivered
    @api.multi
    @api.depends('date_delivered')
    def _days_delivered(self):
        date_format = tools.DEFAULT_SERVER_DATE_FORMAT
        for rec in self:
            if rec.date_delivered:
                if not rec.date_order:
                    _logger.info("No date order -> %s %s", rec.order_id.name, rec.name)
                else:
                    rec.days_delivered = (datetime.strptime(rec.date_delivered, date_format) - datetime.strptime(rec.date_order, date_format)).days

# -- Get date delivered
    """
    Also trigger 'ready for delivery' and 'delivered' triggers for Planned payments
    Send event and sales order line id (may need it in case payment depends on some particular line)
    """
    @api.multi
    @api.depends('move_ids.state')
    def get_date_delivered(self):
        datetime_format = tools.DEFAULT_SERVER_DATETIME_FORMAT
        for rec in self:
            qty_done = qty_assigned = rec.product_uom_qty
            if qty_done == 0:
                return
            last_date = False

            # Check all linked moves and proceed 'assigned' and 'done'
            for move in rec.move_ids:
                move_state = move.state
                if move_state in ['assigned', 'done']:
                    if move.location_dest_id.usage == 'customer':
                        # If move date is older - use it
                        move_date = datetime.strptime(move.date, datetime_format)
                        if not last_date or move_date > last_date:
                            last_date = move_date
                        if move_state == 'done':
                            qty_done -= move.product_uom_qty
                        else:
                            qty_assigned -= move.product_uom_qty
            if qty_done == 0:
                rec.date_delivered = last_date
                # Trigger 'delivered' trigger for related payments
                rec.order_id.payment_planned_ids.trigger_term('3')
            elif qty_assigned == 0:
                rec.date_ready = datetime.today()
                # Trigger 'ready for delivery' trigger for related payments
                rec.order_id.payment_planned_ids.trigger_term('2')

# -- Get date order
    @api.multi
    @api.depends('order_id.date_order')
    def _date_order(self):
        for rec in self:
            rec.date_order = rec.order_id.date_order

# -- Get estimated date
    @api.multi
    @api.depends('date_order', 'state', 'delay')
    def _date_estimated(self):
        date_format = tools.DEFAULT_SERVER_DATE_FORMAT
        date_now = datetime.today()
        for rec in self:
            if rec.delay:
                if rec.state in ['confirmed', 'done']:
                    rec.date_estimated = datetime.strptime(rec.date_order, date_format) + timedelta(days=rec.delay)
                else:
                    rec.date_estimated = date_now + timedelta(days=rec.delay)

# -- Write --
    @api.multi
    def write(self, vals):
        state = vals.get('state', False)
        if state == 'confirmed':
            # If initial delay is in vals - udpate vals
            delay_initial = vals.get('delay', False)
            if delay_initial:
                vals.update('delay_initial', delay_initial)
            else:
                state == 'from_rec'

        super(DMSaleOrderLine, self).write(vals)

        # If initial delay not in vals get it from rec
        if state == 'from_rec':
            for rec in self:
                rec.delay_initial = rec.delay

        # Trigger 'date_accepted'
        if vals.get('date_accepted'):
            for order in self.mapped('order_id'):
                order.payment_planned_ids.trigger_term('4')

# - Split line
    @api.multi
    def button_split_line(self):
        return {
            'type': 'ir.actions.act_window.message',
            'title': _('Confirm your action'),
            'message': _('Pless "Yes" if you want to split this order line into several to be able to transport them separately.\n'
                         'Please be advised that separated lines cannot be merged back later, but it will not affect Sales Order total amount.'),
            'close_button_title': _("No"),
            'buttons': [{
                'type': 'method',
                'name': _('Yes'),
                'model': self._name,
                'method': 'act_split_line',
                'args': [self.ids]
            }]
        }

    @api.multi
    def act_split_line(self):
        self.ensure_one()

        # Check if string is valid
        qty = self.product_uom_qty
        if qty < 2 or self.product_id.type == 'service':
            return

        # Get original line values
        vals = self.env['sale.order.line'].search_read([('id', '=', self.id)])[0]
        # _logger.info('Original vals %s', vals)
        # _logger.info('Order partner id %s', vals['order_partner_id'])

        # Update vals and create new string
        vals['product_uom_qty'] = 1
        self.env['sale.order.line'].create(vals)

        # Update qty of original line
        self.product_uom_qty = qty - 1
        return

# -- Dummy function
    @api.multi
    def _dummy(self):
        return

# -- Calculate discount given
    @api.depends('pricelist_price', 'price_unit', 'product_uom_qty')
    @api.multi
    def get_discount_given(self):
        for rec in self:
            price_unit = rec.price_unit if rec.price_unit else 0
            pricelist_price = rec.pricelist_price if rec.pricelist_price else 0
            if pricelist_price > 1:
                rec.discount_given = 100 - (price_unit / pricelist_price) * 100
                rec.pricelist_subtotal = rec.pricelist_price * rec.product_uom_qty
                rec.discount_subtotal = (rec.pricelist_price - rec.price_unit) * rec.product_uom_qty


# -- Product_id change NEW
    @api.multi
    @api.onchange('product_id')
    def pid_change(self):
        for rec in self:
            parent = rec.order_id
            res = rec.product_id_change(parent.pricelist_id.id, rec.product_id.id, rec.product_uom_qty, False,
                                        rec.product_uos_qty, False, rec.name,
                                        parent.partner_id.id, False, True, parent.date_order, False,
                                        parent.fiscal_position.id, False)
            vals = res.get('value')
            rec.update({
                'product_uos_qty': vals.get('product_uos_qty', 0),
                'name': vals.get('name', False),
                'product_uom': vals.get('product_uom', False),
                'price_unit': vals.get('price_unit', 0),
                'pricelist_price': vals.get('price_unit', 0),
                'product_uos': vals.get('product_uos', False),
                'tax_id': vals.get('tax_id', False),
                'delay': rec.product_id.sale_delay,
                'th_weight': vals.get('weight', rec.product_id.weight)
            })


##########################
#  Sale Order Schet Line #
##########################
class DMSaleOrderSchetLine(models.Model):
    _name = 'sale.order.schet.line'

    def _calc_line_base_price(self, line):
        return line.price_unit * (1 - (line.discount or 0.0) / 100.0)

    def _calc_line_quantity(self, line):
        return line.product_uom_qty

    def _amount_line(self):
        tax_obj = self.pool.get('account.tax')
        cur_obj = self.pool.get('res.currency')
        res = {}
        if self._context is None:
            self._context = {}
        for line in self.browse(self._ids):
            price = self._calc_line_base_price(line)
            qty = self._calc_line_quantity(line)
            taxes = tax_obj.compute_all(self._cr, self._uid, line.my_tax_ids, price, qty,
                                        line.product_id,
                                        line.order_id.partner_id)
            cur = line.order_id.pricelist_id.currency_id
            res[line.id] = cur_obj.round(self._cr, self._uid, cur, taxes['total'])
        return res

    order_id = fields.Many2one(string="Sale order", comodel_name='sale.order', ondelete="cascade", select=True,
                               readonly=True, )
    name = fields.Text(string="Description", required=True, readonly=False)
    so_line_id = fields.Many2one(comodel_name='sale.order.line', string="Sale order lines", ondelete="cascade")
    so_line_add_ids = fields.Many2many('sale.order.line', string="Lines added", ondelete="cascade")
    product_id = fields.Many2one('product.product', string="Product", domain=[('id', '=', so_line_id)], readonly=True,
                                 ondelete='restrict')
    price_unit = fields.Float('Unit Price', required=True, readonly=True)
    product_uom_qty = fields.Float(string="Quantity", readonly=True)
    product_uom = fields.Many2one('product.uom', string="Unit of Measure", required=True, readonly=True)
    price_subtotal = fields.Float(compute='_compute_price_subtotal', string="Subtotal")
    my_tax_ids = fields.Many2many('account.tax', string="Taxes", readonly=True, )
    tax_subtotal = fields.Float(compute='_compute_tax_subtotal', string="Taxes subtotal")
    price_subtotal_tax = fields.Float(compute='_compute_price_subtotal_tax', string="Subtotal with taxes")
    discount = fields.Float(string='Discount (%)', readonly=True)

    @api.depends('price_subtotal', 'my_tax_ids')
    @api.multi
    def _compute_price_subtotal_tax(self):
        for rec in self:
            per = 0
            if rec.my_tax_ids:
                for tax in rec.my_tax_ids:
                    per = rec.price_subtotal * tax.amount
            rec.price_subtotal_tax = rec.price_subtotal + per

    @api.depends('my_tax_ids')
    @api.multi
    def _compute_tax_subtotal(self):
        for rec in self:
            res = 0
            if rec.my_tax_ids:
                for tax in rec.my_tax_ids:
                    res = res + (rec.price_subtotal * tax.amount)
            rec.tax_subtotal = res

    @api.multi
    def _compute_price_subtotal(self):
        vals = {}
        for rec in self:
            for added in rec.so_line_add_ids:
                if vals.get(added):
                    vals[added] += 1
                else:
                    vals[added] = 1
        for rec in self:
            rec.price_subtotal = rec.price_unit * rec.product_uom_qty
            for line in rec.so_line_add_ids:
                rec.price_subtotal = rec.price_subtotal + (line.price_unit / vals.get(line))


#######################
#  Sales Order Wizard #
#######################
class DMSaleOrderWizard(models.TransientModel):
    _name = "dm.sale.order.wizard"

    payment_term = fields.Selection([
        ('0', 'Advanced payment'),
        ('1', 'Order confirmation'),
        ('2', 'Ready for delivery'),
        ('3', 'Delivered'),
        ('4', 'Final acceptance'),
        ('5', 'Fixed date'),
        ('6', 'Other payment'),
    ], string="Payment term")


# -- Trigger term
    @api.multi
    def button_trigger_payment_term(self):
        self.ensure_one()
        orders = self.env['sale.order'].search(['&', ('state', 'not in', ['draft', 'sent', 'cancel']), ('id', 'in', (self._context.get('active_ids')))])
        if orders:
            payment_terms = self.payment_term or ['1', '2', '3', '4', '5', '6']
            for order in orders:
                order.payment_planned_ids.trigger_term(payment_terms)

# -- Redistribute paid amount
    @api.multi
    def button_redistribute_amount_paid(self):
        self.ensure_one()
        orders = self.env['sale.order'].search(['&', ('state', 'not in', ['draft', 'sent', 'cancel']), ('id', 'in', (self._context.get('active_ids')))])
        if orders:
            for order in orders:
                order.sudo().compute_refunded()


# -- Update fixed terms
    @api.multi
    def button_update_term_lines(self):
        self.ensure_one()
        orders = self.env['sale.order'].search(['&', ('state', '!=', 'cancel'), ('id', 'in', (self._context.get('active_ids')))])
        if orders:
            for order in orders:
                if order.payment_term_fixed and len(order.payment_planned_ids) < 1:
                    state = '0' if order.state in ['draft', 'sent'] else '1'
                    for line in order.payment_term.payment_planned_ids:
                        vals = {
                            'order_id': order.id,
                            'state': state,
                            'percent': line.percent,
                            'payment_term': line.payment_term,
                            'days_term': line.days_term
                        }
                        self.env['dm.payment.planned'].sudo().create(vals)
