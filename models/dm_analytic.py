from openerp import models, fields, api, _
import logging
# Logger for debug
_logger = logging.getLogger(__name__)


#############################
# Analytic account/contract #
#############################
class DMAnalytic(models.Model):
    _inherit = "account.analytic.account"

    type = fields.Selection(default="contract")
