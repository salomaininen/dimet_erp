# -*- coding: utf-8 -*-
#
import dm_partner
import avs_phone_number
import dm_company
import dm_user
import dm_product_category
import dm_product_base
import dm_payment_planned
import dm_sale
import dm_lead
import dm_account
import dm_account_voucher
import dm_stock
import dm_calendar
import dm_analytic
import dm_crm_claim
import dm_hr
import dm_res_currency
