from openerp import models, fields, api, _
from openerp.addons import decimal_precision as dp


###############
#  Stock Move #
###############
class DMStockMove(models.Model):
    _inherit = "stock.move"

    sale_line_id = fields.Many2one(
            related='procurement_id.sale_line_id',
            string='Sale Order Line',
            readonly=True,
            store=True,
            ondelete='set null'
    )


###############################################################################
#    Source from StockTransferDetails
#    Credits:
#    Coded by: Yanina Aular <yani@vauxoo.com>
#    Planified by: Gabriela Quilarque <gabriela@vauxoo.com>
#    Audited by: Nhomar Hernandez <nhomar@vauxoo.com>
###############################################################################
class DMTransferDetails(models.TransientModel):

    _inherit = 'stock.transfer_details'

    @api.multi
    def do_detailed_transfer(self):
        """When the transfer was made, a message in log is displayed
        """
        for transfer_detail in self:
            message = _("<b>Picking transfered</b>\n"
                        "<ul><li><b>Date:</b> %s</li>\n"
                        "</ul>\n") % \
                (fields.datetime.now().
                 strftime("%x %X"))
            transfer_detail.picking_id.message_post(body=message)
        return super(DMTransferDetails, self).do_detailed_transfer()


####################
#  Stock Incoterms #
####################
class DMStockIncoterms(models.Model):
    """
    Incoterms
    """
    _inherit = "stock.incoterms"

    transport_required = fields.Boolean(string="Transport required")


################
#  Stock Quant #
################
class DMStockQuant(models.Model):

    _inherit = "stock.quant"

    sale_value = fields.Float(string="Sale Value", compute='_get_sale_value',
                              digits=dp.get_precision('Product Price'),
                              help="Sale Price according to default pricelist for company")
    lot_id = fields.Many2one(required=True)

# - Get sale price
    @api.multi
    def _get_sale_value(self):
        company_id = self.env.user.company_id.id
        pricelist_id = self.env['res.company'].browse([company_id]).pricelist_default.id
        if pricelist_id:
            for rec in self:
                res = self.pool.get('product.pricelist').price_get(self._cr, self._uid, [pricelist_id],
                                                                   rec.id, 1.0)
                rec.sale_value = res[pricelist_id] * rec.qty
