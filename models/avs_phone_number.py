from openerp import models, fields, api
import re


class AvestrusPhoneNumber(models.Model):
    _name = 'avestrus.phone.number'
    _rec_name = 'number_formatted'

    number_type = fields.Selection([
        ('mobile', 'Mobile'),
        ('work', 'Work'),
        ('home', 'Home'),
        ('main', 'Main'),
        ('fax', 'Fax'),
        ('ext', 'Extension'),
        ('custom', 'Custom'),
    ], default='main')

    sequence = fields.Integer(string="Sequence", default=255)
    number_unformatted = fields.Char(required=True, string="Phone number")
    number_formatted = fields.Char(string="Phone number searchable")
    partner_id = fields.Many2one('res.partner', string="Partner", ondelete='cascade')

    _order = 'sequence, id'

# -- Save formatted phone number
    @api.onchange('number_unformatted')
    @api.multi
    def onchange_number_unformatted(self):
        self.ensure_one()
        number_unformatted = self.number_unformatted or False
        if number_unformatted:
            if len(number_unformatted) > 1:
                self.number_formatted = re.sub("[^0-9]", "", number_unformatted)


class AvsCrmPhonecall(models.Model):
    _inherit = "crm.phonecall"

# -- Fields
    section_id = fields.Many2one(default=lambda self: self.env['res.users'].browse([self._uid]).default_section_id.id)
    phone_number = fields.Many2one(string="Phone number", comodel_name='avestrus.phone.number',
                                   domain="[('partner_id','=',partner_id)]")
    partner_user_id = fields.Many2one(string="Partner's Salesman", comodel_name='res.users',
                                      related='partner_id.user_id')
    partner_lead_manager = fields.Many2one(string="Partner's Lead Manager", comodel_name='res.users',
                                           related='partner_id.lead_manager')
    partner_company_id = fields.Many2one(string="Partner's Company", comodel_name='res.company',
                                         related='partner_id.company_id')

# -- Phone number change
    # @api.onchange('phone_number')
    @api.multi
    def phone_number_change(self):
        self.ensure_one()
        self.partner_phone = self.phone_number.number_unformatted

# -- Schedule a meeting
    @api.multi
    def action_make_meeting(self):
        self.ensure_one()
        partner_ids = [self.env['res.users'].browse(self._uid).partner_id.id]
        # partner_ids = [self.pool['res.users'].browse(cr, uid, uid, context=context).partner_id.id]
        if self.partner_id:
            partner_ids.append(self.partner_id.id)
        res = self.env['ir.actions.act_window'].for_xml_id('calendar', 'action_calendar_event')
        # res = self.pool.get('ir.actions.act_window').for_xml_id(cr, uid, 'calendar', 'action_calendar_event', context)
        res['context'] = {
            'default_phonecall_id': self.id,
            'default_partner_id': self.partner_id.id,
            'default_partner_ids': partner_ids,
            'default_user_id': self._uid,
            'default_email_from': self.email_from,
            'default_name': self.name,
        }
        return res

