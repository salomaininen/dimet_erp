from openerp import models, fields, api, _
import logging
# Logger for debug
_logger = logging.getLogger(__name__)


########################
# Account payment term #
########################
class DMAccountPaymentTerm(models.Model):
    _inherit = "account.payment.term"

    # Fields
    payment_planned_ids = fields.One2many(string="Planned payments", comodel_name='dm.payment.planned.template',
                                          inverse_name='term_id')
    fixed = fields.Boolean(string="Terms are fixed", help="User cannot change terms")


###################
# Account journal #
###################
class DMAccountJournal(models.Model):
    _inherit = "account.journal"

    # Fields
    account_number = fields.Char(string="Account number")


###################
# Account invoice #
###################
class DMAccountInvoice(models.Model):
    _inherit = "account.invoice"

# -- Fields
    sale_order_ids = fields.Many2many(string='Sale Orders', comodel_name='sale.order',
                                      relation='sale_order_invoice_rel',
                                      column1='invoice_id', column2='order_id')
# -- Create
    @api.model
    def create(self, vals):
        _logger.info("Invoice CREATE vals: %s", vals)

        # Check if origin name is invoice name and if not prepend it to
        origin = vals.get('origin', False)
        name = vals.get('name', False)
        if not origin:
            res = super(DMAccountInvoice, self).create(vals)
            return res

        vals['name'] = "%s %s" % (origin, name) if name and origin not in name else origin
        vals['reference'] = origin

        return super(DMAccountInvoice, self).create(vals)

# -- Write
    @api.multi
    def write(self, vals):
        super(DMAccountInvoice, self).write(vals)

        # Trigger linked Sales Orders recalculation if state changed
        if vals.get('state', False):
            for rec in self:
                if rec.type == 'out_refund':
                    # Compute refunded
                    rec.sale_order_ids.compute_refunded()


#####################
# Account move line #
#####################
class DMAccountMoveLine(models.Model):
    _inherit = "account.move.line"

# -- Unlink
    @api.multi
    def unlink(self):

        # Get related Sales Orders first
        reconcile_id = self.mapped('reconcile_id') + self.mapped('reconcile_partial_id')
        target_lines = (reconcile_id.line_partial_ids + reconcile_id.line_id) - self
        target_move = target_lines.mapped('move_id')
        invoice = self.env['account.invoice'].sudo().search([('move_id', 'in', target_move.ids)])
        order_ids = invoice.mapped('sale_order_ids')
        # Unlink
        super(DMAccountMoveLine, self).unlink()

        # Compute paid
        order_ids.compute_paid()

    # -- Write
    @api.multi
    def write(self, vals, check=None):
        super(DMAccountMoveLine, self).write(vals)

        # Trigger linked Sales Orders recalculation if state changed
        if vals.get('reconcile_id', False) or vals.get('reconcile_partial_id', False):
            for rec in self:
                if rec.account_id.type == 'receivable':
                    # Invoice is hidden in move != rec.move_id
                    reconcile_id = rec.reconcile_id + rec.reconcile_partial_id
                    if reconcile_id:
                        target_lines = reconcile_id.line_partial_ids + reconcile_id.line_id
                        if not target_lines:
                            continue

                        target_move = target_lines.mapped('move_id')
                        invoice = self.env['account.invoice'].sudo().search([('move_id', 'in', target_move.ids)])
                        _logger.info("Invoice -> %s", invoice)
                        order_ids = invoice.mapped('sale_order_ids')
                        _logger.info("Orders -> %s", order_ids)
                        # Compute paid
                        order_ids.compute_paid()
