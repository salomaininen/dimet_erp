# -*- coding: utf-8 -*-
from openerp import models, fields, api, _, tools, exceptions
from datetime import datetime, timedelta
import logging
import base64
import datetime
# Logger for debug
_logger = logging.getLogger(__name__)


################
# App settings #
################
class DMSettings(models.TransientModel):
    _name = 'dm.settings'
    _inherit = 'res.config.settings'

    default_capacity_gap = fields.Integer(default_model='dm.magnet.selection', string="Capacity reservation(%)",
                                          help="Value in % of maximum lifting capacity subtracted for reserve")
    default_power_gap = fields.Integer(default_model='dm.magnet.selection', string="Power reservation(%)",
                                       help="Value in % of maximum power subtracted for reserve")
    default_capacity_drop = fields.Integer(default_model='dm.magnet', string="Capacity drop(%)",
                                           help="Capacity drop in % of capacity max when magnet \n"
                                                "coil temperature raises from cold to nominal")
    default_power_drop = fields.Integer(default_model='dm.magnet', string="Power drop(%)",
                                        help="Power drop in % of power cold when magnet coil temperature  \n"
                                        "raises from cold to nominal")

# -- Open series
    @api.multi
    def button_series(self):
        return {
            'name': 'Magnet series',
            "views": [[False, "tree"], [False, "form"]],
            'res_model': 'dm.magnet.series',
            'type': 'ir.actions.act_window',
            'target': 'current'
        }


##################
# Sales settings #
##################
class DMSettingsSales(models.TransientModel):
    _name = 'dm.settings.sales'
    _inherit = 'res.config.settings'

    start_date = fields.Datetime(string="Start date for tools")
    default_validity_days = fields.Integer(default_model='sale.order', string="Default quotation validity",
                                           help="Default validity for new quotations in days")
    default_parent_for_invoice = fields.Boolean(default_model='sale.order', string="Use parent company for invoice by default",
                                                help="Default validity for new quotations in days")

# -- Button: Calculate discount given for all Sales order
    @api.multi
    def button_recalculate_base(self):
        return {
            'type': 'ir.actions.act_window.message',
            'title': _('Confirm your action'),
            'message': _('Old base prices will be overwritten!'),
            'close_button_title': _("No"),
            'buttons': [{
                'type': 'method',
                'name': _('Yes'),
                'model': self._name,
                'method': 'set_discount_given',
                'args': [self.ids]
            }]
        }

# -- Calculate discount given for all Sales order
    @api.multi
    def set_discount_given(self):
        self.ensure_one()
        sales_orders = self.env['sale.order'].search([('create_date', '>=', self.start_date)])
        for rec in sales_orders:
            _logger.info("Sales order %s", rec.name)
            amount_untaxed = rec.amount_untaxed
            if amount_untaxed <= 0: continue
            partner_id = rec.partner_id.id
            pricelist_id = rec.pricelist_id.id

            for line in rec.order_line:
                # Calculate only stockable products TODO check services we sell too
                if line.product_id.type == 'product':
                    # Get current price from pricelist
                    product_id = line.product_id.id
                    res = self.pool.get('product.pricelist').price_get(self._cr, self._uid, [pricelist_id],
                                                                       product_id, line.product_uom_qty, partner_id)

                    pricelist_price = res[pricelist_id]
                    _logger.info("Product: %s sale_price: %s pricelist_price: %s", line.product_id.name, line.price_unit, pricelist_price)
                    if pricelist_price > 1:
                        line.pricelist_price = pricelist_price

# -- Button: update Partner, Salesman and Lead Manager for Loss report
    @api.multi
    def button_update_loss_reports(self):

        # Get all reports where partner not set
        loss_reports = self.env['dm.opportunity.loss'].search([('partner_id', '=', False)])

        # Set partner, salesman and lead_manager from related opportunity or sales order
        for loss_report in loss_reports:
            vals = {}
            if loss_report.opportunity_id:
                if loss_report.opportunity_id.partner_id:
                    vals['partner_id'] = loss_report.opportunity_id.partner_id.id
                if loss_report.opportunity_id.user_id:
                    vals['salesman'] = loss_report.opportunity_id.user_id.id
                if loss_report.opportunity_id.lead_manager:
                    vals['lead_manager'] = loss_report.opportunity_id.lead_manager.id
            elif loss_report.sales_order:
                if loss_report.sales_order_id.partner_id:
                    vals['partner_id'] = loss_report.sales_order_id.partner_id.id
                if loss_report.sales_order_id.user_id:
                    vals['salesman'] = loss_report.sales_order_id.user_id.id
                if loss_report.sales_order_id.lead_manager:
                    vals['lead_manager'] = loss_report.sales_order_id.lead_manager.id
            loss_report.write(vals)

# -- Button: update validity for Quotations
    @api.multi
    def button_update_validity(self):
        self.ensure_one()
        date_format = tools.DEFAULT_SERVER_DATETIME_FORMAT
        quotations = self.env['sale.order'].search([('state', 'in', ['draft', 'sent'])])
        for quotation in quotations:
            date_order = datetime.strptime(quotation.date_order, date_format)
            expiration_date = date_order + timedelta(days=self.default_validity_days)
            quotation.write({
                'expiration_date': expiration_date,
                'validity_days': self.default_validity_days
            })


# -- Button: update balance for Sales Orders
    @api.multi
    def button_update_balance(self):
        self.ensure_one()
        self.env['sale.order'].search([('state', 'not in', ['draft', 'sent'])]).compute_balance()


# -- Button: update invoice names
        #   Add ref name to invoice name if not already there
    @api.multi
    def button_update_invoice_name(self):
        self.ensure_one()
        invoices = self.env['account.invoice'].search([('state', '!=', 'cancel')])
        for invoice in invoices:
            name = invoice.name
            origin = invoice.origin
            invoice.name = "%s %s" % (origin, name) if name and origin not in name else origin

# -- Button: update move line names
#   Replace move line name with corresponding invoice name
    @api.multi
    def button_update_move_line_name(self):
        self.ensure_one()
        invoices = self.env['account.invoice'].search([('state', 'in', ['open', 'paid'])])
        for invoice in invoices:
            move_id = invoice.move_id
            if move_id:
                invoice_name = invoice.name
                invoice_origin = invoice.origin
                move_id.ref = invoice_origin if invoice_origin else invoice_name

# -- Button: update product category for Sales Order Lines
        #   Add ref name to invoice name if not already there
    @api.multi
    def button_update_so_line_categ(self):
        self.ensure_one()
        lines = self.env['sale.order.line'].search([('categ_id', '=', False)])
        for line in lines:
                line.categ_id = line.product_id.categ_id

# -- Button: update dates for SO lines
    @api.multi
    def button_update_delay(self):
        self.ensure_one()
        orders = self.env['sale.order'].search([('state', 'not in', ['draft', 'sent', 'cancel'])])
        if orders:
            for order in orders:
                for line in order.order_line:
                    if not line.date_order:
                        line.date_order = order.date_order
                    if not line.delay:
                        line.delay = line.product_id.sale_delay or 0

                    if not line.date_ready and line.date_delivered:
                        line.date_ready = line.date_delivered

# -- Button: link so lines to moves
    @api.multi
    def button_link_so_line_moves(self):
        self.ensure_one()
        for move in self.env['stock.move'].search([('state', '!=', 'cancel')]):
            move.sale_line_id = move.procurement_id.sale_line_id


# -- Button: update Sales Potential
    @api.multi
    def button_sales_potential(self):
        self.ensure_one()
        users = self.env['res.users'].sudo().search([('active', '=', True)])
        users.get_sales_potential()
        users.get_sales_balance()
        users.get_scheduled_balance()


# -- Button: update Lead Planned Revenue
    @api.multi
    def button_planned_revenue(self):
        self.ensure_one()
        """
        opportunities = self.env['crm.lead'].sudo().search(['&', '&',
                                                            ('type', '=', 'opportunity'),
                                                            ('probability', '>', 0),
                                                            ('probability', '<', 100)])
        opportunities.sudo().get_planned_revenue()
        """
        pp = self.env['dm.payment.planned'].sudo().search([('id', '>', 0)])
        for p in pp:
            p.user_id = p.order_id.user_id

